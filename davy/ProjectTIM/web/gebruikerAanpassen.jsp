<%-- 
    Document   : gebruikerAanpassen
    Created on : 9-dec-2017, 9:05:12
    Author     : Jos
--%>

<%@page import="hbo5.it.www.beans.Bedrijf"%>
<%@page import="hbo5.it.www.beans.Sla"%>
<%@page import="hbo5.it.www.beans.Role"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.ArrayList"%>
<%@page import="hbo5.it.www.beans.Ticket"%>
<%@page import="hbo5.it.www.beans.Gebruiker"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Gebruiker Aanpassen</title>
        <link href="css/opmaak.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-thememin.css" rel="stylesheet">
        <link href="css/bootstrapmin.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    </head>
    <body> 
        <%
            Gebruiker g = new Gebruiker();
            //voorlopig word de ingelogde gebruiker aangepast. Moet nog voor andere kunnen.
            if (request.getAttribute("Gebruiker") != null) {
                g = (Gebruiker) request.getAttribute("Gebruiker");
            }
            else
            {
                g.setNaam("Jos");
                g.setLoginid(1);
            } 

            ArrayList<Role> Rollist = (ArrayList<Role>) request.getAttribute("RolLijst");
            ArrayList<Sla> SLAlist = (ArrayList<Sla>) request.getAttribute("SLALijst");
            ArrayList<Bedrijf> Bedrijflist = (ArrayList<Bedrijf>) request.getAttribute("BedrijfLijst");

            
        %>
        <div class="main">
            <form action="GebruikerUpdateServlet" method="post">
                <p>
                    <label>LoginID:</label>
                    <input type="text" name="txtLoginId"  value="<%=g.getLoginid()%>" readonly required>
                </p>
                <p>
                    <label>Naam:</label>
                    <input type="text" name="txtNaam" value="<%=g.getNaam()%>" required>
                </p>
                <p>
                    <label>SLA:</label>
                    <select name="selectSLA" required>
                        <% for(Sla s:SLAlist){ %>
                        <option value="<%=s.getSlaid()%>"><%=s.getOmschrijving()%></option>
                        
                        <%}%>
                    </select>

                </p>
                <p>
                    <label>Rol:</label>
                    <select name="selectRol" required>
                        <% for(Role s:Rollist){ %>
                        <option value="<%=s.getRoleid()%>"><%=s.getOmschrijving()%></option>
                        <%}%>
                    </select>
                </p>            
                <p>
                    <label>Bedrijf:</label>           
                    <select name="selectBedrijf" required>
                        <% for(Bedrijf s:Bedrijflist){ %>
                        <option value="<%=s.getBedrijfid()%>"><%=s.getNaam()%></option>
                        <%}%>
                    </select>
                </p>

                <input type="submit" name="btnAanpassen" value="Aanpassen">
                <input type="submit" name="btnAnnuleren" value="Annuleren">
            </form>
        </div>
    </body>
</html>
