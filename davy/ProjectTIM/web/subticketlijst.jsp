<%@ taglib uri= "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.sql.Array"%>
<%@page import="java.util.ArrayList"%>
<%@page import="hbo5.it.www.dataaccess.DATicket"%>
<%@page import="hbo5.it.www.dataaccess.DAGebruiker"%>
<%@page import="java.util.Calendar"%>
<%@page import="hbo5.it.www.beans.Bericht"%>
<%@page import="hbo5.it.www.beans.Asset"%>
<%@page import="java.util.List"%>
<%@page import="hbo5.it.www.beans.Ticket"%>
<%@page import="hbo5.it.www.beans.Gebruiker"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<!DOCTYPE html>
<html>
<head>
	
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ticket Detail</title>
        <link href="css/opmaak.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-thememin.css" rel="stylesheet">
        <link href="css/bootstrapmin.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="css/chat3.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>



<body>
	<div id="wrapper">
		<div id="header">
			<h2>Lijst SubTickets</h2>
		</div>
	</div>		
	<div id="container">
		<div id="content">			
			<table>                        
				<tr>
					<th>Masterticket</th>
					<th>Subticket</th>
                                        <th>Omschrijving</th>
                                        <th>Status</th>
                                        <th>Startdate</th>
                                        <th>Behandelaar</th>
                                        <th>SLA</th>
                                        
				</tr>				
				<c:forEach var="tempticket" items="${SUB_TICKET }">
					<!-- set up a link for each ticket -->
					<c:url var="tempLink" value="AanmmaakSubTicketServlet">
						<c:param name="command" value="HUIDIG_TICKET"/>                                                
						<c:param name="ticketid" value="${tempticket.id }"/>
					</c:url>
					<tr>						
						<td> ${tempticket.masterticketid} </td>
						<td> ${tempticket.subticketid} </td>
                                                <td> ${tempticket.omschrijving} </td>
                                                <td> ${tempticket.status} </td>
                                                <td> ${tempticket.startdate} </td>
                                                <td> ${tempticket.behandelaar} </td>
                                                <td> ${tempticket.bedrijf} </td>
						
					</tr>				
				</c:forEach>			
			</table>			
		</div>	
	</div>
</body>
</html>