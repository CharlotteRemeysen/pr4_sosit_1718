<%-- 
    Document   : contact
    Created on : 2-dec-2017, 10:40:12
    Author     : c1017444
--%>

<%@page import="java.util.Enumeration"%>
<%@page import="java.util.ArrayList"%>
<%@page import="hbo5.it.www.beans.Ticket"%>
<%@page import="hbo5.it.www.beans.Gebruiker"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Dashboard</title>
        <link href="css/opmaak.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-thememin.css" rel="stylesheet">
        <link href="css/bootstrapmin.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">


        <!-- DASHBOARD DATATABLES -->
        <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/css/jquery.dataTables_themeroller.css">


        <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/jquery.dataTables.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#myTable').DataTable();
            });
        </script>
        <!-- END DASHBOARD DATATABLES -->      
    </head>
    <body>
        <%
            String naam = "";
            if (session.getAttribute("gebruiker") != null) {
                Gebruiker g = (Gebruiker) session.getAttribute("gebruiker");
                naam = g.getNaam();
            }

            ArrayList<Ticket> tickets = new ArrayList<Ticket>();
            if (session.getAttribute("tickets") != null) {
                tickets = (ArrayList<Ticket>) session.getAttribute("tickets");
            }
        %>

        <!-- main layout container -->
    <container class="grid">
        <!-- logo -->
        <div class="logo">logo</div>

        <!-- header met titel van de pagina -->
        <div class="header">
            <img src="Afbeeldingen/list_icon.svg">
            <span class="header-title">Contact - Info</span>
        </div>


        <!-- navigatie -->
        <div class="nav">
            <jsp:include page="menu.jsp"/>
        </div>


        <!-- BODY VAN DE PAGINA -->
        <div class="main">
            s.o.s - IT
        </div>

    </container>
</body>

</html>