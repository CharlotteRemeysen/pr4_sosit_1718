<%-- 
    Document   : Dashboard
    Created on : Nov 18, 2017, 9:00:45 AM
    Author     : Tine
--%>

<%@page import="java.util.Enumeration"%>
<%@page import="java.util.ArrayList"%>
<%@page import="hbo5.it.www.beans.Ticket"%>
<%@page import="hbo5.it.www.beans.Gebruiker"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Dashboard</title>
        <link href="css/opmaak.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-thememin.css" rel="stylesheet">
        <link href="css/bootstrapmin.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">


        <!-- DASHBOARD DATATABLES -->
        <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/css/jquery.dataTables_themeroller.css">


        <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/jquery.dataTables.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#myTable').DataTable();
            });
        </script>
        <!-- END DASHBOARD DATATABLES -->      
    </head>
    <body>
        <%
            String naam = "";
            if (session.getAttribute("gebruiker") != null) {
                Gebruiker g = (Gebruiker) session.getAttribute("gebruiker");
                naam = g.getNaam();
            }

            ArrayList<Ticket> tickets = new ArrayList<Ticket>();
            if (session.getAttribute("tickets") != null) {
                tickets = (ArrayList<Ticket>) session.getAttribute("tickets");
            }
            ArrayList<String[]> tick = new ArrayList<String[]>();
            if (session.getAttribute("tickets") != null) {
                tick = (ArrayList<String[]>) session.getAttribute("tickets");
            }
            
        %>

        <!-- main layout container -->
    <container class="grid">
        <!-- logo -->
        <div class="logo">logo</div>

        <!-- header met titel van de pagina -->
        <div class="header">
            <img src="Afbeeldingen/list_icon.svg">
            <span class="header-title">Overzicht tickets van <%= naam %></span>

        </div>


        <!-- navigatie -->
        <div class="nav">
            <jsp:include page="menu.jsp"/>
        </div>


        <!-- BODY VAN DE PAGINA -->
        <div class="main">
            <!-- filters -->
            <div class="main-container" class="filter">
                <div class="main-title">Filters</div>
                <form>
                    <div class="main-filter">
                        <label>Onderwerp: </label>
                        <input type="text"></input>
                    </div>
                    <div class="main-filter">
                        <label>Naam klant: </label>
                        <input type="text">
                        </select>
                    </div>
                    <div class="main-filter">
                        <label>Status: </label>
                        <select></select>
                    </div>
                    <div class="main-filter">
                        <label>Datum: </label>
                        <select></select>
                    </div>
                    <br>
                    <div class="main-filter">
                        <button type="button" class="btn btn-primary">Zoek</button>
                    </div>
                </form>
            </div>

            <!-- DATATABLE -->
            <div class="main-container" class="datagrid">
                <table id="myTable" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Ticket ID</th>
                            <th>Omschrijving</th>
                            <th>Status</th>
                            <th>Startdatum</th>
                            <th>Einddatum</th>
                            <th>Behandelaar</th>
                            <th>SLA</th>
                        </tr>
                    </thead>

                    <tbody>
                        <% for (Ticket t : tickets) { %>
                        <tr>
                            <% String behandelaar = "";
                                if  (!(t.getBehandelaar()== null)) { behandelaar = t.getBehandelaar().getNaam();}
                            %> 
                            <td><%= t.getTicketid() %></td>
                            <td><%= t.getOmschrijving() %></td>
                            <td><%= t.getStatus() %></td>
                            <td><%= t.getStartdate() %></td>
                            <td><%= t.getEnddate() %> </td>

                            <td><%= behandelaar%></td>
                            <td><%= t.getBedrijf().getNaam()%></td>					

                        </tr>
                       <% } %>
                        </tbody>
                </table>
                <form action=DashboardServlet method="post">
                      <table id="myTable" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Ticket ID</th>
                                <th>Omschrijving</th>
                                <th>Status</th>
                                <th>Startdatum</th>
                                <th>Einddatum</th>
                                <th>Behandelaar</th>
                                <th>Bedrijf</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            <% for (String[] t : tick) {%>
                            <tr>
                                <td><%= t[0]%></td>
                                <td><%= t[1]%></td>
                                <td><%= t[2]%></td>
                                <td><%= t[3]%></td>
                                <td><%= t[4]%> </td>
                                <td><%= t[5]%></td>
                                <td><%= t[6]%></td>
                                <td><button name="buttonView" value="<%= t[0]%>" type="submit">View </button> </td>

                            </tr>
                            <% }%>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>

    </container>
</body>

</html>