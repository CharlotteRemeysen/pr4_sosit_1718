<%-- 
    Document   : menu.jsp
    Created on : 2-dec-2017, 5:02:48
    Author     : Leeuwke
--%>
<ul>
    <li class="nav-item">
        <a href="TicketDetailServlet">
            <span class="nav-title">Overzicht tickets</span>
        </a>
    </li>
    <li class="nav-item">
        <span class="nav-title">Ticket aanmaken</span>
    </li>
    <li class="nav-item">
        <a href="contact.jsp">
            <span class="nav-title">Contact info</span>
        </a>
    </li>
    <li class="nav-item">
        <span class="nav-title">Overzicht behandelaars</span>
    </li>
</ul>
