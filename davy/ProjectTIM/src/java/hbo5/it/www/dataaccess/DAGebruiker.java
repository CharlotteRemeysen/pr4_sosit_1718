/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Bedrijf;
import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Role;
import hbo5.it.www.beans.Sla;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Freedom
 */
public class DAGebruiker {
    //Altijd connection aanmaken + import java.sql.Connection//

    private Connection connection = null;

    //Bovenstaande dingen voor elke klassen toevoegen (aangepast aan de klassen (naam constructor)//
    //********************************************************************************************//
    //Methode om een obj toe te voegen//
    public boolean insertGebruiker(String naam, String loginpassword, int slaid, int roleid, int bedrijfid) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;

        try {
            //tablenaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("insert into gebruiker values ((select max(loginid)+1 from gebruiker),?,?,?,?,?)");
            statement.setString(1, naam);
            statement.setString(2, loginpassword);
            statement.setInt(3, slaid);
            statement.setInt(4, roleid);
            statement.setInt(5, bedrijfid);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }

    // Methode om een obj te verwijderen op ID//
    public boolean deleteGebruiker(int id) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("delete from gebruiker where loginid = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }

    //Methode om obj aan te passen //
    public boolean updateGebruiker(int id, String naam, String loginpassword, int slaid, int roleid, int bedrijfid) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("update gebruiker set naam = ?, loginpassword = ?, slaid = ?, roleid = ?, bedrijfid = ? where loginid = ?");
            statement.setString(1, naam);
            statement.setString(2, loginpassword);
            statement.setInt(3, slaid);
            statement.setInt(4, roleid);
            statement.setInt(5, bedrijfid);
            statement.setInt(6, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }

    //Methode voor obj te zoeken op ID//
    public Gebruiker getGebruikerbyID(int id) {
        connection = ConnectionManager.getConnection();
        Gebruiker obj = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.prepareStatement("select * from gebruiker where loginid = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                obj = new Gebruiker();
                obj.setLoginid(resultSet.getInt("loginid"));
                obj.setNaam(resultSet.getString("naam"));
                obj.setLoginpassword(resultSet.getString("loginpassword"));
                obj.setSlaid(resultSet.getInt("slaid"));
                obj.setRoleid(resultSet.getInt("roleid"));
                obj.setBedrijfid(resultSet.getInt("bedrijfid"));

                DASla dasla = new DASla();
                Sla sla = dasla.getSlabyID(obj.getSlaid());
                obj.setSla(sla);

                DARole darole = new DARole();
                Role role = darole.getRolebyID(obj.getRoleid());
                obj.setRole(role);

                DABedrijf dabedrijf = new DABedrijf();
                Bedrijf bedrijf = dabedrijf.getBedrijfbyID(obj.getBedrijfid());
                obj.setBedrijf(bedrijf);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return obj;
    }

    //Methode voor obj te zoeken op ID//
    public ArrayList<Gebruiker> getGebruikersbyRoleID(int roleID) {
        connection = ConnectionManager.getConnection();
        ArrayList<Gebruiker> gebruikerList = new ArrayList<Gebruiker>();
        Gebruiker obj = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.prepareStatement("select * from gebruiker where roleid = ?");
            statement.setInt(1, roleID);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                obj = new Gebruiker();
                obj.setLoginid(resultSet.getInt("loginid"));
                obj.setNaam(resultSet.getString("naam"));
                obj.setLoginpassword(resultSet.getString("loginpassword"));
                obj.setSlaid(resultSet.getInt("slaid"));
                obj.setRoleid(resultSet.getInt("roleid"));
                obj.setBedrijfid(resultSet.getInt("bedrijfid"));

                DASla dasla = new DASla();
                Sla sla = dasla.getSlabyID(obj.getSlaid());
                obj.setSla(sla);

                DARole darole = new DARole();
                Role role = darole.getRolebyID(obj.getRoleid());
                obj.setRole(role);

                DABedrijf dabedrijf = new DABedrijf();
                Bedrijf bedrijf = dabedrijf.getBedrijfbyID(obj.getBedrijfid());
                obj.setBedrijf(bedrijf);

                gebruikerList.add(obj);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return gebruikerList;
    }

}
