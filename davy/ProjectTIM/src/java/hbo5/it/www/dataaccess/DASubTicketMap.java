

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Asset;
import hbo5.it.www.beans.Ticket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

/**
 *
 * @author Kevin
 */
public class DASubTicketMap {

    //Altijd connection aanmaken + import java.sql.Connection//
    private Connection connection = null;

    

    //Bovenstaande dingen voor elke klassen toevoegen (aangepast aan de klassen (naam constructor)//
    //********************************************************************************************//
    public ArrayList<Ticket> getListSubTicketsbyTicketID(int ticketid) {
        connection = ConnectionManager.getConnection();
        ArrayList<Ticket> resultaat = new ArrayList<Ticket>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //tablenaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("select * from subtickets where masterticketid = ?");
            statement.setInt(1, ticketid);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int subticketid = resultSet.getInt("subticketid");

                DATicket daticket = new DATicket();
                Ticket ticket = daticket.getTicketbyID(subticketid);
                resultaat.add(ticket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }
    
    private void close(Connection myConn, Statement myStmt, ResultSet myRs){
           try {
               if (myRs != null){
                   myRs.close();
               }
               if (myStmt != null){
                   myStmt.close();
               }
               if (myRs != null){
                   myRs.close();
               }
           } 
           catch (Exception ex){
               ex.printStackTrace();
           }          
        
    }
    
    public void addSubticket(int masterticketId, int subticketId) throws Exception {
        Connection myConn = ConnectionManager.getConnection();
        PreparedStatement myStmt = null;
        
        
        
        try {
            //get db connection
           
            
            //create sql for insert
            String sql = "insert into subticket "
                       + "(masterticketid, subTicketId) "
                       + "values (?, ?)";
            myStmt = myConn.prepareStatement(sql);
            //set the param values for the Subticket
            myStmt.setInt(1, masterticketId);
            myStmt.setInt(2, subticketId );
           
            //execute sql insert
            myStmt.execute();
        }
        finally{
            //clean up JDBC objects
            close(myConn, myStmt,null);
        }
        
    }
    public boolean insertSubticket(int ticketid, int subticketid) {

        connection = ConnectionManager.getConnection();
        PreparedStatement statement = null;
        boolean resultaat = true;

        try {
            //tabelnaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("insert into subtickets values (?,?)");
            statement.setInt(1, ticketid);
            statement.setInt(2, subticketid);

            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return resultaat;

    }
}
