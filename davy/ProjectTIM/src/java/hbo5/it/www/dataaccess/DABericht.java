/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Asset;
import hbo5.it.www.beans.Bericht;
import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Groep;
import hbo5.it.www.beans.Ticket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.sql.Date;

/**
 *
 * @author Freedom
 */
public class DABericht {

    //Altijd connection aanmaken + import java.sql.Connection//
    private Connection connection = null;

    //Bovenstaande dingen voor elke klassen toevoegen (aangepast aan de klassen (naam constructor)//
    //********************************************************************************************//
    //Methode om een obj toe te voegen//
    public boolean insertBericht(int contactid, int ticketid, String tekst, boolean isintern) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        int intintern;
        if (isintern == false){
            intintern=0;
        }else{
            intintern=1;
        }
        
        try {
            //tablenaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("insert into bericht values ((select max(berichtid)+1 from bericht),?,?,?,?)");
            statement.setInt(1, contactid);
            statement.setInt(2, ticketid);
            statement.setString(3, tekst);
            statement.setInt(4, intintern);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }

    // Methode om een obj te verwijderen op ID//
    public boolean deleteBericht(int id) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("delete from bericht where berichtid = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }

    //Methode om obj aan te passen //
    public boolean updateBericht(int id, int contactid, int ticketid, String tekst, boolean isintern) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("update bericht set contactid = ?, ticketid = ?, tekst = ?, isintern = ? where berichtid = ?");
            statement.setInt(1, contactid);
            statement.setInt(2, ticketid);
            statement.setString(3, tekst);
            statement.setBoolean(4, isintern);
            statement.setInt(5, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }

    //Methode voor obj te zoeken op ID//
    public Bericht getBerichtbyID(int id) {
        connection = ConnectionManager.getConnection();
        Bericht obj = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.prepareStatement("select * from bericht where berichtid = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                obj = new Bericht();
                obj.setBerichtid(resultSet.getInt("berichtid"));
                obj.setContactid(resultSet.getInt("contactid"));
                obj.setTekst(resultSet.getString("tekst"));
                obj.setIsintern(resultSet.getBoolean("isintern"));
                obj.setTicketid(resultSet.getInt("ticketid"));

                DAGebruiker dacontact = new DAGebruiker();
                Gebruiker contact = dacontact.getGebruikerbyID(obj.getContactid());
                obj.setContact(contact);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return obj;
    }

}
