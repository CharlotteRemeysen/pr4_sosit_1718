/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author c1036807
 */
public class ConnectionManager {
    private static String url = "jdbc:oracle:thin:@itf-oracledb01.thomasmore.be:1521:XE";    
    private static String driverName = "oracle.jdbc.driver.OracleDriver";   
    private static String username = "C1031129";   
    private static String password = "6263";
    private static Connection con;

    public static Connection getConnection() {
        try {
            Class.forName(driverName);
            try {
                con = DriverManager.getConnection(url, username, password);
            } catch (SQLException ex) {
                // log an exception. fro example:
                System.out.println("Failed to create the database connection."); 
            }
        } catch (ClassNotFoundException ex) {
            // log an exception. for example:
            System.out.println("Driver not found."); 
        }
        return con;
    }
    
    //Close methode aanmaken voor connection te sluiten moet ALTIJD gebeuren//
    public static void close() throws SQLException {
        if (con != null) {
            con.close();
        }
    }
    
    
}