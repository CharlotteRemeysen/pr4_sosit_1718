/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Sla;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Freedom
 */
public class DASla {
    //Altijd connection aanmaken + import java.sql.Connection//
    private Connection connection = null;

    //Bovenstaande dingen voor elke klassen toevoegen (aangepast aan de klassen (naam constructor)//
    //********************************************************************************************//

    //Methode om een obj toe te voegen//
    public boolean insertSla(String omschrijving, int duration) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;

        try {
            //tabelnaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("insert into sla values ((select max(slaid)+1 from sla),?,?)");
            statement.setString(1, omschrijving);
            statement.setInt(2, duration);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }

    // Methode om een obj te verwijderen op ID//
    public boolean deleteSla(int id) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("delete from sla where slaid = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }
    
    //Methode om obj aan te passen //
    public boolean updateSla(int id, String omschrijving, int duration){
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("update sla set slaomschrijving = ?, duration = ? where slaid = ?");
            statement.setString(1, omschrijving);
            statement.setInt(2, duration);
            statement.setInt(3, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }
    
    //Methode voor obj te zoeken op ID//
    public Sla getSlabyID(int id){
        connection = ConnectionManager.getConnection();
        Sla obj = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try{
            statement = connection.prepareStatement("select * from sla where slaid = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                obj = new Sla();
                obj.setSlaid(resultSet.getInt("slaid"));
                obj.setOmschrijving(resultSet.getString("slaomschrijving"));
                obj.setDuration(resultSet.getInt("duration"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return obj;
    }
    
    public ArrayList<Sla> returnSlaList()
    {
        connection = ConnectionManager.getConnection();

        ArrayList<Sla> listSlas = new ArrayList<Sla>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Sla obj=null;

        try {
            statement = connection.prepareStatement("select * from SLA");
            resultSet = statement.executeQuery();

            while(resultSet.next()){
                obj = new Sla();
                obj.setSlaid(resultSet.getInt("slaid"));
                obj.setOmschrijving(resultSet.getString("slaomschrijving"));
                obj.setDuration(resultSet.getInt("duration"));
                
                listSlas.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return listSlas;
    }
}
