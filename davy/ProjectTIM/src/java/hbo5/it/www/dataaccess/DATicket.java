
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Asset;
import hbo5.it.www.beans.Bedrijf;
import hbo5.it.www.beans.Bericht;
import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Groep;
import hbo5.it.www.beans.Role;
import hbo5.it.www.beans.Ticket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Freedom
 */
public class DATicket {

    private Connection connection = null;

    //Bovenstaande dingen voor elke klassen toevoegen (aangepast aan de klassen (naam constructor)//
    //********************************************************************************************//
    //Methode om een obj toe te voegen//
    public boolean insertTicket(int behandelaarid, int geopenddoorid, int bedrijfid, String omschrijving, String status, Date startdate) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;

        try {
            //tablenaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("insert into ticket values ((select max(ticketid)+1 from ticket),?,?,?,?,?,?)");
            statement.setInt(1, behandelaarid);
            statement.setInt(2, geopenddoorid);
            statement.setInt(3, bedrijfid);
            statement.setString(4, omschrijving);
            statement.setString(5, status);
            statement.setDate(6, startdate);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }

    // Methode om een obj te verwijderen op ID//
    public boolean deleteTicket(int id) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("delete from ticket where ticketid = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }

    //Methode om obj aan te passen //
    public boolean updateTicket(int id, Integer behandelaarid, int geopenddoorid, int bedrijfid, String omschrijving, String status, Date startdate, Date enddate) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("update ticket set behandelaarid = ?, geopenddoorid = ?, bedrijfid = ?, ticketomschrijving = ?, status=?, startdate=?, enddate =? where ticketid = ?");
            if (behandelaarid == null) {
                statement.setNull(1, Types.INTEGER);
            } else {
                statement.setInt(1, behandelaarid);
            }
            statement.setInt(2, geopenddoorid);
            statement.setInt(3, bedrijfid);
            statement.setString(4, omschrijving);
            statement.setString(5, status);
            statement.setDate(6, startdate);
            statement.setDate(7, enddate);
            statement.setInt(8, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }

    //Methode voor obj te zoeken op ID//
    public Ticket getTicketbyID(int tickid) {

        connection = ConnectionManager.getConnection();
        Ticket obj = null;
        PreparedStatement st = null;
        ResultSet rs = null;        

        String query = "select t.TICKETID, T.BEHANDELAARID, T.GEOPENDDOORID, T.TICKETOMSCHRIJVING, T.STARTDATE, T.ENDDATE, T.STATUS, B.BEDRIJFSNAAM, a.ASSETNAAM, st.SUBTICKETID\n"
                + "from ticket T \n"
                + "left outer join bedrijf B on T.BEDRIJFID = B.BEDRIJFID\n"
                + "left outer join ticketassetmap ta on T.TICKETID = ta.TICKETID left outer join asset a on ta.assetid = a.ASSETID\n"
                + "left outer join subtickets st on T.TICKETID = st.MASTERTICKETID\n"
                + "where t.ticketid = ?";
        
        try {
            st = connection.prepareStatement(query);
            st.setInt(1,tickid );
            rs = st.executeQuery();
            while (rs.next()) {
                obj = this.ticketopvullen(rs);         
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }

        return obj;

    }

    public Ticket ticketopvullen(ResultSet rs) {

        ResultSet rs2 = null;
        PreparedStatement st2 = null;
        Ticket obj = null;
        ArrayList<Ticket> subtickets = new ArrayList<>();
        ArrayList<Bericht> berichten = new ArrayList<>();
        ArrayList<Asset> assets = new ArrayList<>();

        String subticketqeury = "Select t.ticketomschrijving, t.status\n"
                + "from subtickets st \n"
                + "left outer join ticket t\n"
                + "on t.TICKETID = st.SUBTICKETID\n"
                + "where st.MASTERTICKETID = ?";

        String berichtenquery = "Select st.TEKST, g.naam\n"
                + "from bericht st \n"
                + "left outer join gebruiker g\n"
                + "on st.contactid = g.loginid\n"
                + "where st.ticketid = ?";

        String assetquery = "Select a.assetnaam\n"
                + "from ticketassetmap tam\n"
                + "left outer join asset a\n"
                + "on tam.assetid = a.assetid\n"
                + "where tam.ticketid = ?";

        try {

            obj = new Ticket();
            Bedrijf b = new Bedrijf();
            b.setNaam(rs.getString("BEDRIJFSNAAM"));
            obj.setBedrijf(b);
            obj.setBehandelaarid(rs.getInt("BEHANDELAARID"));
            obj.setEnddate(rs.getDate("EndDATE"));
            obj.setGeopenddoorid(rs.getInt("GEOPENDDOORID"));
            obj.setOmschrijving(rs.getString("TICKETOMSCHRIJVING"));
            obj.setStartdate(rs.getDate("STARTDATE"));
            obj.setStatus(rs.getString("STATUS"));
            obj.setTicketid(rs.getInt("TICKETID"));

            st2 = connection.prepareStatement(subticketqeury);
            st2.setInt(1, obj.getTicketid());
            rs2 = st2.executeQuery();

            while (rs2.next()) {
                Ticket e = new Ticket();
                e.setOmschrijving(rs2.getString("TICKETOMSCHRIJVING"));
                e.setStatus(rs2.getString("STATUS"));
                subtickets.add(e);
            }
            obj.setListtickets(subtickets);

            st2 = connection.prepareStatement(berichtenquery);
            st2.setInt(1, obj.getTicketid());
            rs2 = st2.executeQuery();

            while (rs2.next()) {
                Bericht e = new Bericht();
                Gebruiker g = new Gebruiker();
                e.setTekst(rs2.getString("TEKST"));
                e.setContact(g);
                g.setNaam(rs2.getString("NAAM"));

                berichten.add(e);
            }
            obj.setListberichten(berichten);

            st2 = connection.prepareStatement(assetquery);
            st2.setInt(1, obj.getTicketid());
            rs2 = st2.executeQuery();

            while (rs2.next()) {
                Asset a = new Asset();
                a.setNaam(rs2.getString("ASSETNAAM"));
                assets.add(a);
            }
            obj.setListassets(assets);
        } catch (SQLException e) {
            e.getMessage();
        }
        return obj;

    }

    public ArrayList<Ticket> getTicketsByBedrijfID(int bedrijfid) {
        connection = ConnectionManager.getConnection();

        ArrayList<Ticket> listtickets = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.prepareStatement("select * from ticket "
                    + "where bedrijfid = ?");
            statement.setInt(1, bedrijfid);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                DATicket daticket = new DATicket();
                Ticket ticket = daticket.getTicketbyID(resultSet.getInt("ticketid"));
                listtickets.add(ticket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return listtickets;
    }

    public ArrayList<Ticket> getTicketsByBehandelaarID(int loginid) {

        connection = ConnectionManager.getConnection();
        ArrayList<Ticket> listtickets = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.prepareStatement("select * from ticket where behandelaarid = ?");
            statement.setInt(1, loginid);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {

                DATicket daticket = new DATicket();
                Ticket ticket = daticket.getTicketbyID(resultSet.getInt("ticketid"));
                listtickets.add(ticket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return listtickets;
    }

    public ArrayList<Ticket> getTickets() {
        connection = ConnectionManager.getConnection();

        ArrayList<Ticket> listtickets = new ArrayList<>();
        PreparedStatement statement = null;

        try {
            ResultSet resultSet = null;
            statement = connection.prepareStatement("select * from ticket");
            
            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                DATicket daticket = new DATicket();
                Ticket ticket = daticket.getTicketbyID(resultSet.getInt("ticketid"));
                listtickets.add(ticket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return listtickets;
    }

    public ArrayList<Integer> getListTicketIDs() {
        connection = ConnectionManager.getConnection();

        ArrayList<Integer> listtickets = new ArrayList<Integer>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.prepareStatement("select ticketid from ticket");

            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                listtickets.add(resultSet.getInt("ticketid"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return listtickets;
    }

    public ArrayList<String[]> getTicketsforDashBoard() {
        connection = ConnectionManager.getConnection();

        ArrayList<String[]> listtickets = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.prepareStatement("select * from ticket left "
                    + "outer join gebruiker on ticket.behandelaarid = gebruiker.loginid "
                    + "left outer join bedrijf on ticket.bedrijfid = bedrijf.bedrijfid ");
            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                String[] ticket = new String[7];

                ticket[0] = Integer.toString(resultSet.getInt("ticketid"));
                ticket[1] = resultSet.getString("ticketomschrijving");
                ticket[2] = resultSet.getString("status");
                ticket[3] = resultSet.getString("startdate");
                ticket[4] = resultSet.getString("enddate");
                if (resultSet.getString("behandelaarid") == null) {
                    ticket[5] = "";
                } else {
                    ticket[5] = resultSet.getString("naam");
                }
                ticket[6] = resultSet.getString("bedrijfsnaam");

                listtickets.add(ticket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return listtickets;
    }

    public ArrayList<String[]> getTicketsforDashBoard(int login, int bedrijf, int rol) {
        connection = ConnectionManager.getConnection();

        ArrayList<String[]> listtickets = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {

            if (rol == 1) {

                statement = connection.prepareStatement("select * from ticket left "
                        + "outer join gebruiker on ticket.behandelaarid = gebruiker.loginid "
                        + "left outer join bedrijf on ticket.bedrijfid = bedrijf.bedrijfid "
                        + "where ticket.bedrijfid = ?");
                statement.setInt(1, bedrijf);
            } else if (rol == 2) {

                statement = connection.prepareStatement("select * from ticket left "
                        + "outer join gebruiker on ticket.behandelaarid = gebruiker.loginid "
                        + "left outer join bedrijf on ticket.bedrijfid = bedrijf.bedrijfid "
                        + "where ticket.geopenddoorid = ? or ticket.behandelaarid = ?"
                        + "or ticket.behandelaarid is null");
                statement.setInt(1, login);
                statement.setInt(2, login);

            } else {
                statement = connection.prepareStatement("select * from ticket left "
                        + "outer join gebruiker on ticket.behandelaarid = gebruiker.loginid "
                        + "left outer join bedrijf on ticket.bedrijfid = bedrijf.bedrijfid ");
            }

            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                String[] ticket = new String[7];

                ticket[0] = Integer.toString(resultSet.getInt("ticketid"));
                ticket[1] = resultSet.getString("ticketomschrijving");
                ticket[2] = resultSet.getString("status");
                ticket[3] = resultSet.getString("startdate");
                ticket[4] = resultSet.getString("enddate");
                if (resultSet.getString("behandelaarid") == null) {
                    ticket[5] = "";
                } else {
                ticket[5] = resultSet.getString("naam");
                }
                ticket[6] = resultSet.getString("bedrijfsnaam");

                listtickets.add(ticket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return listtickets;
    }

}
