/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www;

import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Ticket;
import hbo5.it.www.dataaccess.DASubTicketMap;
import hbo5.it.www.dataaccess.DATicket;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author hannes
 */
@WebServlet(name = "UpdateTicketServlet",
        urlPatterns = {"/UpdateTicketServlet"}
)

public class UpdateTicketServlet extends HttpServlet {

    DATicket daticket = null;

    @Override
    public void init() throws ServletException {
        try {

            if (daticket == null) {
                daticket = new DATicket();
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //zoekt de ticket nodig om te updaten.
        Ticket ticket = null;

        DASubTicketMap Subticket = new DASubTicketMap();

        HttpSession session = request.getSession();
        Gebruiker gebr = null;
        try {
            if (session.getAttribute("ticket") != null) {
                ticket = ((Ticket) session.getAttribute("ticket"));
            } else {
                session.setAttribute("errorcode", 40);
                throw new Exception();
            }
            if (session.getAttribute("gebruiker") != null) {
                gebr = ((Gebruiker) session.getAttribute("gebruiker"));
            } else {
                session.setAttribute("errorcode", 41);
                throw new Exception();
            }
        } catch (Exception e) {
            response.sendRedirect("ErrorPaginaServlet");
        }

        //zoekt gegevens van huidige gebruiker
        //Lijst van subtickets die aan ticket id zouden hangen
        //datum conversie omdat updateTicket geen util.date aanvaart, enkel sql.date
        java.util.Date utilStartDate = ticket.getStartdate();
        java.sql.Date sqlStartDate = new java.sql.Date(utilStartDate.getTime());
        java.util.Date utilEndDate = ticket.getEnddate();
        java.sql.Date sqlEndDate = new java.sql.Date(utilEndDate.getTime());

        // BIJ UPDATE VAN STATUS ////////////////////////////////////////////////////
        try {
            if (request.getParameter("selectStatus") != null) {
                //haalt de gekozen status van de jsp pagina
                String selectedStatus = request.getParameter("selectStatus");

                //indien ticket op status "open" wordt gezet, wordt de behandelaar verwijderd
                Integer behandelaarID = ticket.getBehandelaarid();
                if (selectedStatus.equals("Open")) {
                    behandelaarID = null;
                } else {
                    behandelaarID = ticket.getBehandelaarid();
                }

                //als ticket op status "Gesloten" wordt gezet, gekeken of er een subticket is aan gekoppelt indien ja geeft hij een boodschap en veranderd hij niets indien ja gebeurt er niets
                if (selectedStatus.equals("Gesloten")) {
                    ArrayList<Ticket> listsubticket = Subticket.getListSubTicketsbyTicketID(ticket.getTicketid());

                    if (!listsubticket.isEmpty()) {
                        for (Ticket t : listsubticket) {
                            if (!t.getStatus().equals("Gesloten")) {
                                session.setAttribute("getAlert", "Yes");
                            }
                        }
                        response.sendRedirect("TicketDetailServlet");
                        return;
                    }
                }

            }

                    //indien ticket open of gesloten staat, verandert de status naar "in behandeling"
                    String ticketStatus = "";
                    if (!ticket.getStatus().equals("In behandeling")) {
                        ticketStatus = "In behandeling";
                        Integer behandelaarID = ticket.getBehandelaarid();
                        String selectedStatus = request.getParameter("selectStatus");
                        //update van de ticket met de gekozen status
                        daticket.updateTicket(ticket.getTicketid(), behandelaarID, ticket.getGeopenddoorid(), ticket.getBedrijfid(), ticket.getOmschrijving(), selectedStatus, sqlStartDate, sqlEndDate);
                    } // BIJ UPDATE VAN BEHANDELAAR ///////////////////////////////////////////////
                    else if (request.getParameter("selectBehandelaar") != null) {
                        //haalt de login nummer van de geselecteerde behandelaar
                        int selectedLoginID = Integer.parseInt(request.getParameter("selectBehandelaar"));
                        String selectedStatus = request.getParameter("selectStatus");
                        Integer behandelaarID = ticket.getBehandelaarid();

                        //indien ticket op status "in behandeling" wordt gezet, wordt de behandelaar gekoppeld aan de huidige behandelaar
                        if (gebr.getRoleid() == 2 && selectedStatus.equals("In behandeling")) {
                            behandelaarID = (int) gebr.getLoginid();
                        }
                        //indien behandelaarid 0 is, omzetten naar null
                        if (behandelaarID != null) {
                            if (behandelaarID == 0) {
                                behandelaarID = null;
                            }
                        }
                        //update van de ticket met de gekozen status
                        daticket.updateTicket(ticket.getTicketid(), behandelaarID, ticket.getGeopenddoorid(), ticket.getBedrijfid(), ticket.getOmschrijving(), selectedStatus, sqlStartDate, sqlEndDate);
                    } // BIJ UPDATE VAN BEHANDELAAR ///////////////////////////////////////////////
                    else if (request.getParameter("selectBehandelaar") != null) {
                        //haalt de login nummer van de geselecteerde behandelaar
                        int selectedLoginID = Integer.parseInt(request.getParameter("selectBehandelaar"));

                        //indien ticket open of gesloten staat, verandert de status naar "in behandeling"
                        ticketStatus = "";
                        if (!ticket.getStatus().equals("In behandeling")) {
                            ticketStatus = "In behandeling";
                        } else {
                            ticketStatus = ticket.getStatus();
                        }
                        if (selectedLoginID == 0) {
                            ticketStatus = "Open";
                        }
                        //update van de ticket met de gekozen behandelaar
                        daticket.updateTicket(ticket.getTicketid(), selectedLoginID, ticket.getGeopenddoorid(), ticket.getBedrijfid(), ticket.getOmschrijving(), ticketStatus, sqlStartDate, sqlEndDate);
                         
                    } else {
                        if (request.getParameter("selectStatus") == null) {
                            session.setAttribute("errorcode", 42);
                            throw new Exception();
                        }
                        if (request.getParameter("selectBehandelaar") == null) {
                            session.setAttribute("errorcode", 43);
                            throw new Exception();
                        }
                    }
                    response.sendRedirect("TicketDetailServlet");
                }catch (Exception e) {
            response.sendRedirect("ErrorPaginaServlet");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
