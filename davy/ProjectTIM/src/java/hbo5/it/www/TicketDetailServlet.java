
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www;

import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Ticket;
import hbo5.it.www.dataaccess.DAGebruiker;
import hbo5.it.www.dataaccess.DATicket;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Freedom
 */
@WebServlet(name = "TicketDetailServlet",
        urlPatterns = {"/TicketDetailServlet"}
)

public class TicketDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Ticket ticket = null;
    DATicket daticket = null;
    DAGebruiker dagebruiker = null;

    @Override
    public void init() throws ServletException {
        try {

            if (daticket == null) {
                daticket = new DATicket();
            }
            if (dagebruiker == null) {
                dagebruiker = new DAGebruiker();
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        RequestDispatcher rd = null;
        ArrayList<String> behandelaarSelectionList = new ArrayList<String>();

        //testdata
        int gebruikerNummer = 0;
        if (request.getParameter("radioButton") != null) {
            gebruikerNummer = Integer.parseInt(request.getParameter("radioButton"));
        } else {
            Gebruiker g = (Gebruiker) session.getAttribute("gebruiker");
            gebruikerNummer = g.getRoleid();
        }
        Gebruiker gebruiker = dagebruiker.getGebruikerbyID(gebruikerNummer);
        session.setAttribute("gebruiker", gebruiker);
        Ticket testticket = daticket.getTicketbyID(2);
        session.setAttribute("ticket", testticket);
        //einde testdata
        try {
            if (session.getAttribute("gebruiker") != null && session.getAttribute("ticket") != null) {
                ticket = (Ticket) session.getAttribute("ticket");
                session.setAttribute("ticket", ticket);

                //Zoek actieve gebruiker
                Gebruiker gebr = (Gebruiker) session.getAttribute("gebruiker");
                // call de methode die een option list van behandelaars returned, afhankelijk van het type gebruiker dat is ingelogd
                behandelaarSelectionList = this.returnBehandelaarListByUser(gebr);

                // Einde invulling van behandelaars
                request.setAttribute("behandelaaropties", behandelaarSelectionList);
            if (session.getAttribute("getAlert") != null) {
                String error = "Er hangen nog subtickets aan het ticket";
                session.setAttribute("error", error);
            }

            

                rd = request.getRequestDispatcher("/ticketdetail.jsp");
                rd.forward(request, response);
            } else {
                if(session.getAttribute("gebruiker")==null){
                    session.setAttribute("errorcode", 30);
                    throw new Exception();
                }
                if(session.getAttribute("ticket")==null){
                    session.setAttribute("errorcode", 31);
                    throw new Exception();
                }
            }
            
        } catch (Exception e) {
            response.sendRedirect("ErrorPaginaServlet");
        }

        /*Kijken of er een gebruiker en ticket is doorgeven*/
    }

    public ArrayList<String> returnBehandelaarListByUser(Gebruiker gebr) {
        Gebruiker b = new Gebruiker();     
        dagebruiker.getGebruikerbyID(ticket.getBehandelaarid());

        ArrayList<String> behandelaarSelectionList = new ArrayList<>();
        //Maak een lijst van alle behandelaars aan
        ArrayList<Gebruiker> behandelaarsList = dagebruiker.getGebruikersbyRoleID(2);
        //Vul een lege lijst met behandelaars, afhankelijk van de rol van de aangemelde gebruiker
        String optieLeegSelected = "<option selected>" + "Geen behandelaar" + "</option>";
        String optieActieveBehandelaar = "";

        // voor klant en behandelaar
        if (gebr.getRoleid() == 1 || gebr.getRoleid() == 2) {
            //indien ticket geen behandelaar heeft: laat 'geen behandelaar' zien
           
            if (b == null) {
                behandelaarSelectionList.add(optieLeegSelected);

            } //indien ticket een behandelaar heeft, laat actieve behandelaar zien
            else {
                optieActieveBehandelaar = "<option selected value='" + b.getLoginid() + "'>" + b.getNaam() + "</option>";
                behandelaarSelectionList.add(optieActieveBehandelaar);
            }
        } // indien beheerder is aangemeld
        else if (gebr.getRoleid() == 3) {
            if ((Integer) ticket.getBehandelaarid() == 0) {
                behandelaarSelectionList.add(optieLeegSelected);
            }
            for (Gebruiker behandelaar : behandelaarsList) {
                //indien aangemeld als beheerder: vul de non-actieve behandelaars aan in de lijst van behandelaars
                if (behandelaar.getLoginid() != ticket.getBehandelaarid()) {
                    String optieBehandelaar = "<option value='" + behandelaar.getLoginid() + "'>" + behandelaar.getNaam() + "</option>";
                    behandelaarSelectionList.add(optieBehandelaar);
                } //vul actieve behandelaar in met tag "selected"
                else {
                    String optieBehandelaar = "<option selected value='" + ticket.getBehandelaar().getLoginid() + "'>" + ticket.getBehandelaar().getNaam() + "</option>";
                    behandelaarSelectionList.add(optieBehandelaar);
                }
            }
        }

        return behandelaarSelectionList;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
