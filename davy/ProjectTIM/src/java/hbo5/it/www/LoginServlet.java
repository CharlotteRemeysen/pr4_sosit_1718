/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www;

import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Ticket;
import hbo5.it.www.dataaccess.DAGebruiker;
import hbo5.it.www.dataaccess.DATicket;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.InputMismatchException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author hannes
 */
@WebServlet(name = "LoginServlet",
        urlPatterns = {"/LoginServlet"})

public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private DAGebruiker dagebruiker = null;
    private DATicket daticket = null;

    @Override
    public void init() throws ServletException {
        try {

            if (dagebruiker == null) {
                dagebruiker = new DAGebruiker();
            }
            if (daticket == null) {
                daticket = new DATicket();
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int loginid;
        String loginpassword;
        HttpSession session = request.getSession();
        RequestDispatcher rd = null;

        //Check if you are already logged in.
        if (session.getAttribute("login") != null) {
            if (session.getAttribute("login").equals(true)) {
                rd = request.getRequestDispatcher("dashboard.jsp");
                rd.forward(request, response);
            }
        }

        try {
            //controle op lege velden
            if (request.getParameter("txtLoginId").isEmpty() || request.getParameter("txtLoginPassword").isEmpty()) {
                session.setAttribute("errorcode", 10);
                throw new Exception();
                //throw new InputMismatchException("Geef uw gebruikersnaam en wachtwoord in.");
            } else {

                try {
                    loginid = Integer.parseInt(request.getParameter("txtLoginId"));
                    loginpassword = request.getParameter("txtLoginPassword");
                } catch (Exception e) {
                    session.setAttribute("errorcode", 11);
                    throw new Exception();
                    //throw new NumberFormatException("Geef een geldige gebruikersID in.");
                }

                Gebruiker g = dagebruiker.getGebruikerbyID(loginid);
               // ArrayList<String[]> tickets = new ArrayList<String[]>();

                //indien gebruiker niet wordt gevonden via id, maak error aan dat gebruiker niet bestaat
                if (g == null) {
                    session.setAttribute("errorcode", 12);
                    throw new Exception();
                    //throw new NullPointerException("Gebruiker bestaat niet.");

                } //indien wachtwoord niet gelijk is, maak error aan
                else if (!g.getLoginpassword().equals(loginpassword)) {
                    session.setAttribute("errorcode", 13);
                    throw new Exception();
                    //throw new Exception("Wachtwoord is niet correct.");
                } //indien wachtwoord klopt, geef gebruiker door als attribuut, en geef login = true door aan de sessie
                else {
//                    switch (g.getRoleid()) {
//                        case 1:
//                        case 2:
//                            tickets = daticket.getTicketsforDashBoard(g.getLoginid(), g.getBedrijfid(), g.getRoleid());
//                            break;
//                        case 3:
//                            tickets = daticket.getTicketsforDashBoard();
//                            break;
//                    }

//                    session.setAttribute("rol", g.getLoginid());
                    session.setAttribute("login", true);
                    session.setAttribute("gebruiker", g);
//                    session.setAttribute("tickets", tickets);

                    rd = request.getRequestDispatcher("dashboard.jsp");
                    rd.forward(request, response);
                }

            }
            //geef de mogelijke error terug naar login.jsp

        } catch (Exception e) {
            response.sendRedirect("ErrorPaginaServlet");
        }

        //rd = request.getRequestDispatcher("login.jsp");
        //rd.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
