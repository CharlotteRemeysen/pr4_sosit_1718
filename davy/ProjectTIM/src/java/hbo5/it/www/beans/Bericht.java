/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.beans;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author Freedom
 */
public class Bericht {
    
    private int berichtid;
    private int contactid;
    private int ticketid;
    private String tekst;
    private boolean isintern;
    private Gebruiker contact;
    private Date posttijd;

    public int getBerichtid() {
        return berichtid;
    }

    public int getContactid() {
        return contactid;
    }

    public int getTicketid() {
        return ticketid;
    }

    public String getTekst() {
        return tekst;
    }

    public boolean isIsintern() {
        return isintern;
    }


    public Gebruiker getContact() {
        return contact;
    }



    public void setBerichtid(int berichtid) {
        this.berichtid = berichtid;
    }

    public void setContactid(int contactid) {
        this.contactid = contactid;
    }

    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public void setIsintern(boolean isintern) {
        this.isintern = isintern;
    }


    public void setContact(Gebruiker contact) {
        this.contact = contact;
    }


    public Date getPosttijd() {
        return posttijd;
    }

    public void setPosttijd(Date posttijd) {
        this.posttijd = posttijd;
    }
    
    
    
}
