/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.beans;

/**
 *
 * @author Freedom
 */
public class Groep {
    
    private int groepid;
    private int slaid;
    private Sla sla;
    private String omschrijving;

    public int getGroepid() {
        return groepid;
    }

    public int getSlaid() {
        return slaid;
    }

    public Sla getSla() {
        return sla;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setGroepid(int groepid) {
        this.groepid = groepid;
    }

    public void setSlaid(int slaid) {
        this.slaid = slaid;
    }

    public void setSla(Sla sla) {
        this.sla = sla;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }
    
    
    
}
