/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.beans;

/**
 *
 * @author Kevin
 */
public class Role {
    
    private int roleid;
    private String omschrijving;

    public int getRoleid() {
        return roleid;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public Role() {
        this.roleid = 0;
        this.omschrijving = "";
    }
    
    
    
}
