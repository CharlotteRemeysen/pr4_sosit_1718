/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.beans;

import java.util.ArrayList;
/**
 *
 * @author Freedom
 */
public class Asset {
    
    private int assetid;
    private int beheerderid;
    private int groepid;
    private String naam;
    private String omschrijving;
    private Gebruiker beheerder;
    private Groep groep;
    private ArrayList<Asset> listassets;

    public void setListassets(ArrayList<Asset> listassets) {
        this.listassets = listassets;
    }

    public int getAssetid() {
        return assetid;
    }

    public int getBeheerderid() {
        return beheerderid;
    }

    public int getGroepid() {
        return groepid;
    }



    public String getOmschrijving() {
        return omschrijving;
    }

    public Gebruiker getBeheerder() {
        return beheerder;
    }

    public Groep getGroep() {
        return groep;
    }



    public void setAssetid(int assetid) {
        this.assetid = assetid;
    }

    public void setBeheerderid(int beheerderid) {
        this.beheerderid = beheerderid;
    }

    public void setGroepid(int groepid) {
        this.groepid = groepid;
    }



    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public void setBeheerder(Gebruiker beheerder) {
        this.beheerder = beheerder;
    }

    public void setGroep(Groep groep) {
        this.groep = groep;
    }



    public ArrayList<Asset> getListassets() {
        return listassets;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }
    
}
