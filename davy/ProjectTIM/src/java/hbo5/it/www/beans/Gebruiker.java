/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.beans;

/**
 *
 * @author Freedom
 */
public class Gebruiker {
    
    private int loginid;
    private String naam;
    private String loginpassword;
    private int slaid;
    private int roleid;
    private int bedrijfid;
    private Sla sla;
    private Role role;
    private Bedrijf bedrijf;

    public int getLoginid() {
        return loginid;
    }

    public String getNaam() {
        return naam;
    }

    public String getLoginpassword() {
        return loginpassword;
    }

    public int getSlaid() {
        return slaid;
    }

    public int getRoleid() {
        return roleid;
    }

    public int getBedrijfid() {
        return bedrijfid;
    }

    public Sla getSla() {
        return sla;
    }

    public Role getRole() {
        return role;
    }

    public Bedrijf getBedrijf() {
        return bedrijf;
    }

    public void setLoginid(int loginid) {
        this.loginid = loginid;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setLoginpassword(String loginpassword) {
        this.loginpassword = loginpassword;
    }

    public void setSlaid(int slaid) {
        this.slaid = slaid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public void setBedrijfid(int bedrijfid) {
        this.bedrijfid = bedrijfid;
    }

    public void setSla(Sla sla) {
        this.sla = sla;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setBedrijf(Bedrijf bedrijf) {
        this.bedrijf = bedrijf;
    }

    
}
