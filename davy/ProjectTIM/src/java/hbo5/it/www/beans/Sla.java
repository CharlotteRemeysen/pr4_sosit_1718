/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.beans;

/**
 *
 * @author Freedom
 */
public class Sla {
    
    private int slaid;
    private String omschrijving;
    private int duration;

    public int getSlaid() {
        return slaid;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public int getDuration() {
        return duration;
    }

    public void setSlaid(int slaid) {
        this.slaid = slaid;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    
    
    
}
