/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Freedom
 */
public class Ticket {

    private int ticketid;
    private int behandelaarid;
    private int bedrijfid;
    private int geopenddoorid;
    private String omschrijving;
    private String status;
    private Date startdate;
    private Date enddate;
    private Gebruiker behandelaar;
    private Bedrijf bedrijf;
    private Gebruiker geopenddoor;
    private ArrayList<Asset> listassets;
    private ArrayList<Ticket> listtickets;
    private ArrayList<Bericht> listberichten;
    
    public ArrayList<Ticket> getListtickets() {
        return listtickets;
    }

    public void setListtickets(ArrayList<Ticket> listtickets) {
        this.listtickets = listtickets;
    }
    
    public ArrayList<Asset> getListassets() {
        return listassets;
    }

    public void setListassets(ArrayList<Asset> listassets) {
        this.listassets = listassets;
    }
    
    public Gebruiker getBehandelaar() {
        return behandelaar;
    }



    public Gebruiker getGeopenddoor() {
        return geopenddoor;
    }

    public void setBehandelaar(Gebruiker behandelaar) {
        this.behandelaar = behandelaar;
    }



    public void setGeopenddoor(Gebruiker geopenddoor) {
        this.geopenddoor = geopenddoor;
    }

    public int getTicketid() {
        return ticketid;
    }

    public int getBehandelaarid() {
        return behandelaarid;
    }


    public int getGeopenddoorid() {
        return geopenddoorid;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public String getStatus() {
        return status;
    }

    public Date getStartdate() {
        return startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    public void setBehandelaarid(int behandelaarid) {
        this.behandelaarid = behandelaarid;
    }



    public void setGeopenddoorid(int geopenddoorid) {
        this.geopenddoorid = geopenddoorid;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public Bedrijf getBedrijf() {
        return bedrijf;
    }

    public void setBedrijf(Bedrijf bedrijf) {
        this.bedrijf = bedrijf;
    }

    public int getBedrijfid() {
        return bedrijfid;
    }

    public void setBedrijfid(int bedrijfid) {
        this.bedrijfid = bedrijfid;
    }

    public ArrayList<Bericht> getListberichten() {
        return listberichten;
    }

    public void setListberichten(ArrayList<Bericht> listberichten) {
        this.listberichten = listberichten;
    }

    
    
}
