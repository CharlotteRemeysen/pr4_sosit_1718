
package hbo5.it.www;

import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Ticket;
import hbo5.it.www.dataaccess.DATicket;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Nomad
 */


@WebServlet(name = "DashboardServlet",
        urlPatterns = {"/DashboardServlet"})

public class DashboardServlet extends HttpServlet {

    private DATicket daticket = null;

    @Override
    public void init() throws ServletException {
        try {
            if (daticket == null) {
                daticket = new DATicket();
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();

        Gebruiker g = (Gebruiker) session.getAttribute("gebruiker");

        List<String[]> tickets = daticket.getTicketsforDashBoard(g.getLoginid(), g.getBedrijfid(), g.getRoleid());

        session.setAttribute("tickets", tickets);
        session.setAttribute("gebruiker", g);

        RequestDispatcher rd = request.getRequestDispatcher("dashboard.jsp");
        rd.forward(request, response);


    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher rd;
        HttpSession session = request.getSession();
        Gebruiker gebruiker = (Gebruiker)session.getAttribute("gebruiker");
        if (request.getParameter("knopTicketAanmaken") != null) {	

      	request.setAttribute("gebruiker", gebruiker);
      	rd = request.getRequestDispatcher("ticketaanmaken.jsp");        	
	rd.forward(request, response);

        }
        
        if (request.getParameter("knopoverzicht") != null) {	
        session.setAttribute("gebruiker", gebruiker);
        response.sendRedirect("ticketaanmaken.jsp");
        }
        
        if (request.getParameter("knopcontactinfo") != null) {	
        session.setAttribute("gebruiker", gebruiker);
        response.sendRedirect("ticketaanmaken.jsp");
        }
        
        if (request.getParameter("knopoverzichtbehandelaars") != null) {
        session.setAttribute("gebruiker", gebruiker);
        response.sendRedirect("ticketaanmaken.jsp");
        }
        
    }

    
 

       

    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();

        String button = request.getParameter("buttonView");

        session.setAttribute("ticketid", button);

        response.sendRedirect("TicketDetailServlet");
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    

        
   

}
