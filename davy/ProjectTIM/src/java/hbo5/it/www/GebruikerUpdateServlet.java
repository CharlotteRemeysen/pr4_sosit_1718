/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www;

import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Sla;
import hbo5.it.www.beans.Ticket;
import hbo5.it.www.dataaccess.DAGebruiker;
import hbo5.it.www.dataaccess.DASla;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author DrJos
 */
@WebServlet(name = "GebruikerUpdateServlet", urlPatterns = {"/GebruikerUpdateServlet"})
public class GebruikerUpdateServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   
    DAGebruiker dagebruiker = null;
    DASla dasla=null;

    @Override
    public void init() throws ServletException {
        try {

            if (dagebruiker == null) {
                dagebruiker = new DAGebruiker();
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
        
        try {

            if (dasla == null) {
                dasla = new DASla();
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();

        if (request.getParameter("btnAanpassen")!=null) 
        {
            String naam= (String) request.getParameter("txtNaam");

            Integer gebruikerid=Integer.parseInt(request.getParameter("txtLoginId"));
            Gebruiker g = dagebruiker.getGebruikerbyID(gebruikerid);
            int slaid= Integer.parseInt(request.getParameter("selectSLA"));
            int roleid= Integer.parseInt(request.getParameter("selectRol"));
            int bedrijfid= Integer.parseInt(request.getParameter("selectBedrijf"));        
            dagebruiker.updateGebruiker(gebruikerid,naam,g.getLoginpassword(),slaid,roleid,bedrijfid);            
        }
        
        if (request.getParameter("btnAanmaken")!=null) 
        {
            String naam= (String) request.getParameter("txtNaam");

            String paswoord = (String) request.getParameter("txtPaswoord1");
            String paswoordcontrole = (String) request.getParameter("txtPaswoord2");
            try
            {
                if (!paswoord.equals(paswoordcontrole)) 
                {
                    session.setAttribute("errorcode", 14);
                    throw new Exception();
                    //throw new Exception("Wachtwoord is niet correct.");
                } 
                
            }

            catch (Exception e) 
            {
                response.sendRedirect("ErrorPaginaServlet");
            }
            
            int slaid= Integer.parseInt(request.getParameter("selectSLA"));
            int roleid= Integer.parseInt(request.getParameter("selectRol"));
            int bedrijfid= Integer.parseInt(request.getParameter("selectBedrijf"));
            
            dagebruiker.insertGebruiker(naam, paswoord, slaid, roleid, bedrijfid);
        }
        
//        //Request dispatcher voor final version als behanderlaarslijst aanwezig is
//        RequestDispatcher rd = request.getRequestDispatcher("behandelaarslijst.jsp");
//        rd.forward(request, response);

        // voorlopige dispatcher om terug naar de vorige pagina te gaan
        RequestDispatcher rd = request.getRequestDispatcher("ticketdetailTestPagina.jsp");
        rd.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
