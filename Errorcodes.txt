Errors Codes:

10 = Geen gebruikersID of password ingevuld.
11 = Geef een geldige gebruikerID in.
12 = Gebruiker bestaat niet.
13 = Verkeerd wachtwoord.

20 = geen gebruiker bij het laden van berichtservlet.
21 = geen ticket bij het laden van berichtservlet.
22 = tekst bericht is leeg.

30 = geen gebruiker bij het laden van ticketdetailservlet.
31 = geen ticket bij het laden van ticketdetailservlet.

40 = geen ticket bij het laden van updateticketservlet.
41 = geen gebruiker bij het laden van updateticketservlet.
42 = geen status doorgeven in parameter van ticketdetail.jsp
43 = geen behandelaar doorgeven in parameter van ticketdetail.jsp

666 = fout op database
