-- Dummy data met script
-- Sla values

insert into SLA values (1, 'SLA voorbeeld', 10);
insert into SLA values (2, 'SLA voorbeeld 2', 20);
insert into SLA values (3, 'SLA nog minder korte termijn', 30);
insert into SLA values (4, 'SLA lange termijn', 100);

-- Bedrijf values

insert into BEDRIJF values (1, 'NIKE', 'Sportartikelen');
insert into BEDRIJF values (2, 'LENOVO', 'Computerartikelen');
insert into BEDRIJF values (3, 'KUL', 'Katholieke univeriteit leuven');
insert into BEDRIJF values (4, 'NYRSTAR','Zink en koper productie');
insert into BEDRIJF values (5, 'VALVE', 'Gaming');
insert into BEDRIJF values (6, 'IKEA', 'Voor elke hipsterwoning kom even langs');
insert into BEDRIJF values (7, 'ASML', 'Productie van Processoren');
insert into BEDRIJF values (8, 'KODAK','Fotografie');
insert into BEDRIJF values (9, 'GOOGLE', 'They know everything');
insert into BEDRIJF values (10, 'Apple', 'To expensive bit apple branded elektronics');
insert into BEDRIJF values (11, 'WALMART', 'Get fat for the cheap');
insert into BEDRIJF values (12, 'TOYOTA','Racing like a granny');

-- Gebruiker values

insert into GEBRUIKER values (1, 'Kevin', '1234',1,1,1);
insert into GEBRUIKER values (2, 'Hannes', '2345', 2, 2, 2);
insert into GEBRUIKER values (3, 'Seppe', '3456', 1, 3, 3);
insert into GEBRUIKER values (4, 'Davy', '1234',1,1,1);
insert into GEBRUIKER values (5, 'Jeroen', '1234',1,1,1);
insert into GEBRUIKER values (6, 'Slobdan', '1234',1,1,1);
insert into GEBRUIKER values (7, 'Timmy', '1234',1,1,1);

-- Groep values

insert into GROEP values (1, 1, 'IT', 'software and more');
insert into GROEP values (2, 1, 'Administratie', 'administratie and more');
insert into GROEP values (3, 1, 'Aankoop', 'Wij kopen alles!');

-- Asset values

insert into ASSET values (1,3,1,'database pc', 'Alle databases');
insert into ASSET values (2,3,1,'Computers', 'Alle pc');
insert into ASSET values (3,3,1,'Autos', 'Alle autos');
insert into ASSET values (4,3,1,'Gebouwen', 'Alle gebouwen');
insert into ASSET values (5,3,1,'ProductDatabase', 'Alles over onze producten komt in deze database');
insert into ASSET values (6,3,1,'Laptop Seppe', 'Mooie Lenovo E570');
insert into ASSET values (7,3,1,'Auto Hannes', 'Renault Clio');
insert into ASSET values (8,3,1,'Tuinhuis', 'Hier zit alles om de tuin te verzorgen in');
insert into ASSET values (9,3,1,'VerkoopDatabase', 'Alles met verkoop komt in deze database');
insert into ASSET values (10,3,1,'Rommel computer HIK', 'Dell Optiplex 7010');
insert into ASSET values (11,3,1,'Auto Seppe', 'Volvo V40');
insert into ASSET values (12,3,1,'Cafetaria', 'Voor eten en drinken moet je hier zijn');


-- Ticket values

insert into TICKET values (1,2, 6, 2,'windows updates werken niet meer','Open', '2009-11-20','2018-02-20');
insert into TICKET values (2,2, 3, 2,'Antivirus software','Open', '2009-11-20','2018-02-20');
insert into TICKET values (3,2, 4, 6,'lekkende kraan in cafetaria','Open', '2009-11-20','2018-02-20');
insert into TICKET values (4,2,2,6, 'nieuwe kraan bestellen voor ikea cafetaria','open', '2009-11-20','2018-02-20');

-- Ticketassetmap Values

insert into Ticketassetmap values (1,10); -- ticket 1 (windows updates) refereert naar tickets 10 (computer hik)
insert into Ticketassetmap values (2,6);
insert into Ticketassetmap values (3,12);

-- Bericht Values

INSERT INTO BERICHT VALUES (1, 2, 1, 'Hannes moet beter weten dan windows te gebruiken en linux erop zetten','1');
INSERT INTO BERICHT VALUES (2, 2, 1, 'Gelieve windows updates af te zetten tot probleem verholpen is danku','1');

-- Subassetmap Values

insert into Subassetmap values (2,6); -- Computers als master en laptop Seppe als subasset
insert into Subassetmap values (3,7); -- Auto als master en auto Hannes als subasset
insert into Subassetmap values (4,8); -- Gebouwen als master en Tuinhuis als subasset

-- Subtickets Values

insert into Subtickets values (3,4); -- lekkende kraan ticket is master van nieuwe kraan bestellen.