ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY/MM/DD';

drop table role cascade constraints;
drop table bedrijf cascade constraints;
drop table gebruiker cascade constraints;
drop table sla cascade constraints;
drop table groep cascade constraints;
drop table asset cascade constraints;
drop table ticket cascade constraints;
drop table bericht cascade constraints;
drop table ticketassetmap cascade constraints;
drop table subassetmap cascade constraints;
drop table subtickets cascade constraints;
drop sequence role_seq;


create table role(
roleid        int         primary key,
roleomschrijving  varchar(50)  not null
);

create sequence role_seq;

insert into role values (role_seq.nextval,'klant');
insert into role values (role_seq.nextval,'behandelaar');
insert into role values (role_seq.nextval,'beheerder');


create table sla(
slaid           int 	        not null,
slaomschrijving	varchar(50)  not null,
duration		int   not null, 
primary key (slaid)
);

create table bedrijf(
bedrijfid int not null,
bedrijfsnaam varchar(50) not null,
bedrijfomschrijving varchar(50) not null,
primary key (bedrijfid)
);

create table gebruiker(
loginid           int 	     not null,
naam              varchar(50)  not null,
loginpassword     varchar(20)  not null, 
slaid             int  not null,
roleid            int  not null,
bedrijfid int not null,
primary key (loginid),
foreign key (slaid) references sla (slaid),
foreign key (roleid) references role (roleid),
foreign key (bedrijfid) references bedrijf(bedrijfid)
);

create table groep(
groepid int not null,
slaid int not null,
groepnaam varchar(50) not null,
groepomschrijving varchar(50) not null,
primary key (groepid), 
foreign key (slaid) references sla (slaid)
);

create table asset(
assetid  int not null,
beheerderid int not null,
groepid int not null,
assetnaam varchar(50) not null,
assetomschrijving varchar(50) not null,
primary key (assetid),
foreign key (beheerderid) references gebruiker (loginid),
foreign key (groepid) references groep (groepid)
);

create table ticket(
ticketid int not null,
behandelaarid int null,
geopenddoorid int not null,
bedrijfid int not null,
ticketomschrijving varchar(50) null,
status varchar(30) not null,
startdate date not null,
enddate date null,
primary key (ticketid),
foreign key (behandelaarid) references gebruiker (loginid),
foreign key (geopenddoorid) references gebruiker (loginid),
foreign key (bedrijfid) references bedrijf (bedrijfid)
);

create table ticketassetmap(
ticketid int not null,
assetid int not null,
primary key (ticketid, assetid),
foreign key (ticketid) references ticket (ticketid),
foreign key (assetid) references asset (assetid)
); 

create table bericht(
berichtid int not null,
contactid int not null,
ticketid int not null,
tekst varchar(200) not null,
isintern int not null,
-- posttijd date not null,
primary key (berichtid),
foreign key (contactid) references gebruiker (loginid),
foreign key (ticketid) references ticket (ticketid)
);

create table subassetmap(
masterassetid int not null,
subassetid int not null,
primary key (masterassetid, subassetid),
foreign key (masterassetid) references asset (assetid),
foreign key (subassetid) references asset (assetid)
);

create table subtickets(
masterticketid int not null,
subticketid int not null,
primary key (masterticketid, subticketid),
foreign key (masterticketid) references ticket (ticketid),
foreign key (subticketid) references ticket (ticketid)
);


