<%-- 
    Document   : errorpagina
    Created on : 9-dec-2017, 9:05:14
    Author     : Kevin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ticket Detail</title>
        <link href="css/opmaak.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-thememin.css" rel="stylesheet">
        <link href="css/bootstrapmin.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="css/chat3.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <% Integer errorcode = null;
            String errormessage = null;
            if (session.getAttribute("errorcode") != null && session.getAttribute("errormessage") != null) {
                errorcode = (Integer) session.getAttribute("errorcode");
                errormessage = (String) session.getAttribute("errormessage");
            }
        %>
    </head>
    <body>
    <container class="grid">
        <!-- logo -->
        <div class="logo">logo</div>

        <!-- header met titel van de pagina -->
        <div class="header">
            <img src="Afbeeldingen/list_icon.svg">
            <span class="header-title">Error!</span>
        </div>

        <!-- navigatie -->
        <div class="nav">
            <ul>
                <li class="nav-item">
                    <span class="nav-title">Overzicht tickets</span>
                </li>
                <li class="nav-item">
                    <span class="nav-title">Ticket aanmaken</span>
                </li>
                <li class="nav-item">
                    <span class="nav-title">Contact info</span>
                </li>
                <li class="nav-item">
                    <span class="nav-title">Overzicht behandelaars</span>
                </li>
            </ul>
        </div>

        <!-- BODY VAN DE PAGINA -->
        <div class="main">
            <div id="testID"></div>

            <!-- TICKET -->
            <div class="main-container">
                <div>Er is een fout opgetreden!</div>
                <div>Errorcode: <%=errorcode%> </div>
                <div>Errormessage: <%=errormessage%> </div>

                <form action="ErrorPaginaServlet" method="post">
                    <div class="form-group">
                        <input type="submit" class="btn btn-default pull-right" name="button" value="Terug">
                    </div>
                </form>
            </div>
        </div>
    </container>
</body>
</html>
