<%-- 
    Document   : ticketdetail
    Created on : 18-nov-2017, 14:30:53
    Author     : hannes
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Array"%>
<%@page import="java.util.ArrayList"%>
<%@page import="hbo5.it.www.dataaccess.DATicket"%>
<%@page import="hbo5.it.www.dataaccess.DAGebruiker"%>
<%@page import="java.util.Calendar"%>
<%@page import="hbo5.it.www.beans.Bericht"%>
<%@page import="hbo5.it.www.beans.Asset"%>
<%@page import="java.util.List"%>
<%@page import="hbo5.it.www.beans.Ticket"%>
<%@page import="hbo5.it.www.beans.Gebruiker"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ticket Detail</title>
        <link href="css/opmaak.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-thememin.css" rel="stylesheet">
        <link href="css/bootstrapmin.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="css/chat3.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%
            //controle op type gebruiker: indien klant wordt via javascript een aantal zaken verborgen
            //test data

            //delete testdata
            int roleID = 0;
            String error = "";
            if(session.getAttribute("error")!=null){
                error = session.getAttribute("error").toString();
            }              
            Ticket ticket = null;
            Gebruiker g = null;
            
            if (session.getAttribute("gebruiker") != null && session.getAttribute("ticket") != null) {
                ticket = (Ticket) session.getAttribute("ticket");
                g = (Gebruiker) session.getAttribute("gebruiker");
                roleID = g.getRoleid();
            }
        %>
        <script type="text/javascript">
            //Bij inloggen van klant: verwijder dropdown en opmaak van selectelementen status en behandelaar 

            window.onload = function () {
                if (<%=roleID%> == 1) {
                    console.log("disabling client elements")
                    disableKlantVisibility();

                } else if (<%=roleID%> == 2) {
                    disableBehandelaarVisibility();
                }


            }

            function disableKlantVisibility() {
                console.log("disableklantvisibility function")
                var klantElementHidden = document.getElementsByClassName("klantElementHidden");
                for (var i = 0; i < klantElementHidden.length; i++) {
                    console.log("test")
                    var element = klantElementHidden.item(i);
                    element.style.display = "none";
                    element.style.appearance = "none";
                }                
                var klantElementen = document.getElementsByClassName("disableKlantSelect");
                for (var i = 0; i < klantElementen.length; i++) {
                    console.log("klantelements")
                    var element = klantElementen.item(i);
                    element.disabled = true;
                    element.style.appearance = "none";
                    element.style.border = "none";
                    element.style.webkitAppearance = "none";
                    element.style.MozAppearance = "none";
                    element.style.overflow = "hidden";
                    element.style.width = "120%";
                    element.style.backgroundColor = "white";
                    element.style.MozMarginStart = "-6px";
                    element.style.webkitMarginStart = "-1px";
                    element.style.textIndent = "1px";
                    element.style.textOverflow = "";
                }
                
            }

            function disableBehandelaarVisibility() {
                var klantElementen = document.getElementsByClassName("disableBehandelaarSelect");
                for (var i = 0; i < klantElementen.length; i++) {
                    var element = klantElementen.item(i);
                    element.disabled = true;
                    element.style.appearance = "none";
                    element.style.border = "none";
                    element.style.webkitAppearance = "none";
                    element.style.MozAppearance = "none";
                    element.style.overflow = "hidden";
                    element.style.width = "120%";
                    element.style.backgroundColor = "white";
                    element.style.MozMarginStart = "-6px";
                    element.style.webkitMarginStart = "-1px";
                    element.style.textIndent = "1px";
                    element.style.textOverflow = "";
                }
            }
            
    
        </script>       
    </head>

    <body>




        <!-- main layout container -->
    <container class="grid">
        <!-- logo -->
        <div class="logo">logo</div>

        <!-- header met titel van de pagina -->
        <div class="header">
            <img src="Afbeeldingen/list_icon.svg">
            <span class="header-title">Ticket - <%=ticket.getTicketid()%></span>
        </div>

        <!-- navigatie -->
        <div class="nav">
            <ul>
                <li class="nav-item">
                    <span class="nav-title">Overzicht tickets</span>
                </li>
                <li class="nav-item">
                    <span class="nav-title">Ticket aanmaken</span>
                </li>
                <li class="nav-item">
                    <span class="nav-title">Contact info</span>
                </li>
                <li class="nav-item">
                    <span class="nav-title">Overzicht behandelaars</span>
                </li>
            </ul>
        </div>

        <!-- BODY VAN DE PAGINA -->
        <div class="main">
            <div id="testID"></div>

            <!-- TICKET -->
            <div class="main-container">
                <div class="main-title">Ticket Details</div>
                <div class="detail-ticket-wrapper">
                    <!-- titels van rijen (div die links is uitgelijnd) -->
                    <div class="detail-titles">
                        <div>Omschrijving:</div>
                        <div>Status:</div>
                        <div>Startdatum: </div>
                        <div>Einddatum:</div>
                        <div>Klant:</div>
                        <div>Behandelaar:</div>

                    </div>

                    <!-- inhoud van de ticketdetails -->
                    <div class="detail-inhoud">
                        <div><%=ticket.getOmschrijving()%></div>

                        <!-- form om status en behandelaar te kunnen wijzigen -->
                        <form name="TicketUpdateStatus" action="UpdateTicketServlet" method="POST">
                            <%
                                String[] selectString = new String[3];

                                if (ticket.getStatus()
                                        .equals("Open")) {
                                    selectString[0] = "selected";
                                }

                                if (ticket.getStatus()
                                        .equals("In behandeling")) {
                                    selectString[1] = "selected";
                                }

                                if (ticket.getStatus()
                                        .equals("Gesloten")) {
                                    selectString[2] = "selected";
                                }
                            %>


                            <select name="selectStatus" class="disableKlantSelect" onchange="this.form.submit()">
                                <option <%=selectString[0]%> tag="1">Open</option>
                                <option <%=selectString[1]%> tag="2">In behandeling</option>
                                <option <%=selectString[2]%> tag="3">Gesloten</option>
                            </select>
                            <label name="Error">
                                <%=error %>                              
                            </label>
                        </form>
                            
                            

                        <div><%=ticket.getStartdate()%></div>
                        <div><%=ticket.getEnddate()%></div>
                        <div><%=ticket.getBedrijf().getNaam()%></div>
                        <form  name="TicketUpdateBehandelaar" action="UpdateTicketServlet" method="POST">
                            <select name="selectBehandelaar" class="disableKlantSelect disableBehandelaarSelect" onchange="this.form.submit()">
                                <!-- invullen met lijst van behandelaars-->
                                <%
                                    String outputOptionString = "";
                                    ArrayList<String> behandelaarOpties = (ArrayList<String>) request.getAttribute("behandelaaropties");
                                    for (String s : behandelaarOpties) {
                                        outputOptionString += s;
                                    }
                                %>
                                <%=outputOptionString%>
                            </select>
                        </form>
                        <!-- einde van formulier -->

                    </div>
                </div>

                <!-- assets titel en inhoud -->
                <div class="detail-ticket-wrapper">
                    <div class="detail-titles">
                        <div>Assets:</div>
                    </div>
                    <div class="detail-inhoud">
                        <!-- invullen met lijst van assets gekoppeld aan deze ticket -->
                        <%ArrayList<Asset> listassets = ticket.getListassets();
                            if (!listassets.isEmpty()) {
                                for (Asset item : listassets) {%>
                        <div>
                            <span><%=item.getNaam()%></span>                        
                        </div><%;
                            }
                        } else {%>
                        <div><span>geen assets</span></div><%}%>
                    </div>
                </div>

                <!-- subtickets titel en inhoud -->
                <div class="detail-ticket-wrapper">
                    <div class="detail-titles">
                        <div>Subtickets:</div>
                    </div>
                    <div class="detail-inhoud">
                        <!-- invullen met lijst van subtickets -->
                        <% ArrayList<Ticket> listsubtickets = ticket.getListtickets();
                            if (!listsubtickets.isEmpty()) {
                                for (Ticket item : listsubtickets) {
                        %>
                        <div>
                            <span>Status : </span>
                            <span><%=item.getStatus()%></span>
                            <span> - Van : </span>
                            <span><%=item.getOmschrijving()%></span>
                        </div><%;
                            }
                        } else {%>  
                        <div>
                            <span>geen subtickets</span>
                        </div><%}%>
                    </div>
                </div>

                <!-- END TICKET -->


                <!-- BUTTONS -->

                <button type="button" onclick="window.location.href='subticketlijst.jsp'; return false" class="btn btn-primary detail-button" class="behandelaar">Subticket aanmaken</button>

                <br/>





                <!-- SUBTICKET TOEWIJZEN -->

                <form name="Subtickettoewijzen" action="SubticketaddServlet" method="post" class="klantElementHidden">
                    
                    <input type="submit" value="Subticket toewijzen" class="btn btn-primary detail-button" /> Subticket ID: <input type="integer" name="SubTicketID">
                    
                    
                                      
                </form>

            </div>
            <!-- END BUTTONS -->

            <!-- BERICHTENBOX -->
            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>
                            <i class="fa fa-circle text-green"></i><%=g.getNaam()%></h4>
                    </div>
                    <div class="portlet-widgets">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="chat" class="panel-collapse collapse in">
                    <div>
                        <div class="portlet-body chat-widget" style="overflow-y: auto; width: auto; height: 300px;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="text-center text-muted small"><%java.sql.Time time = new java.sql.Time(Calendar.getInstance().getTime().getTime());%><%=time%></p>
                                </div>
                            </div>

                            <!-- BERICHTEN -->

                            <!-- blok van één bericht -->
                            <% ArrayList<Bericht> listberichten = ticket.getListberichten();
                                if (!listberichten.isEmpty()) {
                                    if (g.getRoleid() == 1) {
                                        for (Bericht item : listberichten) {
                                            if (!item.isIsintern()) {%>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle" src="https://lorempixel.com/30/30/people/1/" alt="">
                                        </a>
                                        <div class="media-body">                                            
                                            <h4 class="media-heading"><%=item.getContact().getNaam()%>
                                            </h4>
                                            <p><%=item.getTekst()%></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr><%;
                                    }
                                }
                            } else {
                                for (Bericht item : listberichten) {%>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle" src="https://lorempixel.com/30/30/people/1/" alt="">
                                        </a>
                                        <div class="media-body">                                            
                                            <h4 class="media-heading"><%=item.getContact().getNaam()%>
                                            </h4>
                                            <p><%=item.getTekst()%></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <%}
                                }
                            } else {%>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle" src="https://lorempixel.com/30/30/people/1/" alt="">
                                        </a>
                                        <div class="media-body">                                            
                                            <h4 class="media-heading">
                                                <span class="small pull-right"></span>
                                            </h4>
                                            <p>geen berichten</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr><%}
                            %>
                        </div>
                    </div>

                    <div class="portlet-footer">
                        <form action="BerichtServlet" method="post">
                            <div class="form-group">
                                <!--                                <input type="tekst" name="berichttekst" value="enter message ...">-->
                                <textarea class="form-control" name="berichttekst" placeholder="Enter message..."></textarea>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="isintern" class="disableKlantSelect"><%if (g.getRoleid() == 1) {%><p hidden="true">Intern Bericht</p><%} else {%><p>Intern Bericht</p><%}%>
                                <input type="submit" class="btn btn-default pull-right" value="Send">
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- EINDE BERICHTENBOX -->
        </div>
    </container>
</body>

</html>