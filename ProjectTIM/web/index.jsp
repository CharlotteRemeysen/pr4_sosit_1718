<%-- 
    Document   : index
    Created on : 2-dec-2017, 5:13:45
    Author     : Leeuwke
--%>

<%@page import="java.sql.Array"%>
<%@page import="java.util.ArrayList"%>
<%@page import="hbo5.it.www.dataaccess.DATicket"%>
<%@page import="hbo5.it.www.dataaccess.DAGebruiker"%>
<%@page import="java.util.Calendar"%>
<%@page import="hbo5.it.www.beans.Bericht"%>
<%@page import="hbo5.it.www.beans.Asset"%>
<%@page import="java.util.List"%>
<%@page import="hbo5.it.www.beans.Ticket"%>
<%@page import="hbo5.it.www.beans.Gebruiker"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ticket Detail</title>
        <link href="css/opmaak.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-thememin.css" rel="stylesheet">
        <link href="css/bootstrapmin.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="css/chat3.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%
            //controle op type gebruiker: indien klant wordt via javascript een aantal zaken verborgen
            //test data

            //delete testdata
            int roleID = 0;
            Ticket ticket = null;
            Gebruiker g = null;
            if (session.getAttribute("gebruiker") != null && session.getAttribute("ticket") != null) {
                ticket = (Ticket) session.getAttribute("ticket");
                g = (Gebruiker) session.getAttribute("gebruiker");
                roleID = g.getRoleid();
            }
        %>
        <script type="text/javascript">
            //Bij inloggen van klant: verwijder dropdown en opmaak van selectelementen status en behandelaar 
            
            window.onload = function () {
                if(<%=roleID%> == 1){
                    disableKlantVisibility();
                }
                else if(<%=roleID%> == 2){
                    disableBehandelaarVisibility();
                }
                
                
            }

            function disableKlantVisibility() {
                var klantElementen = document.getElementsByClassName("disableKlantSelect");
                for (var i = 0; i < klantElementen.length; i++) {
                    var element = klantElementen.item(i);
                    element.disabled = true;
                    element.style.appearance = "none";
                    element.style.border = "none";
                    element.style.webkitAppearance = "none";
                    element.style.MozAppearance = "none";
                    element.style.overflow = "hidden";
                    element.style.width = "120%";
                    element.style.backgroundColor = "white";
                    element.style.marginLeft = "-4px";
                }
            }
            
            function disableBehandelaarVisibility() {
                var klantElementen = document.getElementsByClassName("disableBehandelaarSelect");
                for (var i = 0; i < klantElementen.length; i++) {
                    var element = klantElementen.item(i);
                    element.disabled = true;
                    element.style.appearance = "none";
                    element.style.border = "none";
                    element.style.webkitAppearance = "none";
                    element.style.MozAppearance = "none";
                    element.style.overflow = "hidden";
                    element.style.width = "120%";
                    element.style.backgroundColor = "white";
                    element.style.marginLeft = "-4px";
                }
            }
        </script>
    </head>

    <body>
        <!-- main layout container -->
    <container class="grid">
        <!-- logo -->
        <div class="logo">logo</div>

        <!-- header met titel van de pagina -->
        <div class="header">
            <img src="Afbeeldingen/list_icon.svg">
            <span class="header-title">Ticket - - - -</span>
        </div>

        <!-- navigatie -->
        <div class="nav">
            -
        </div>

        <!-- BODY VAN DE PAGINA -->
        <div class="main">
            <jsp:include page="login.jsp"/>
        </div>
    </container>
</body>

</html>