<%-- 
    Document   : login
    Created on : 2-nov-2017, 11:40:09
    Author     : hannes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SOSIT - Login</title>
    </head>
    <body>
        
        <h1>Login</h1>
        <form action="LoginServlet" method="post">
            <p>
                <label>GebruikersID:</label>
                <input type="text" name="txtLoginId" required>
            </p>
            <p>
                <label>Wachtwoord:</label>
                <input type="text" name="txtLoginPassword" required>
            </p>
            <input type="submit" name="loginSubmit" value="Login">

        </form>
        <%
            if (request.getAttribute("error") != null) {
                String error = (String) request.getAttribute("error");
        %><p><%=error%></p><%
                    }
        %>
        
    </body>
</html>
