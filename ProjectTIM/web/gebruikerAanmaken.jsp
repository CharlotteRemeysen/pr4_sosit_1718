<%-- 
    Document   : gebruikerAanmaken
    Created on : 16-dec-2017, 12:25:29
    Author     : DrJos
--%>
<%@page import="hbo5.it.www.beans.Bedrijf"%>
<%@page import="hbo5.it.www.beans.Sla"%>
<%@page import="hbo5.it.www.beans.Role"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.ArrayList"%>
<%@page import="hbo5.it.www.beans.Ticket"%>
<%@page import="hbo5.it.www.beans.Gebruiker"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Gebruiker Aanmaken</title>
        <link href="css/opmaak.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-thememin.css" rel="stylesheet">
        <link href="css/bootstrapmin.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    </head>
    <body> 
        <%
            ArrayList<Role> Rollist = (ArrayList<Role>) request.getAttribute("RolLijst");
            ArrayList<Sla> SLAlist = (ArrayList<Sla>) request.getAttribute("SLALijst");
            ArrayList<Bedrijf> Bedrijflist = (ArrayList<Bedrijf>) request.getAttribute("BedrijfLijst");

            
        %>
        <div class="main">
            <form action="GebruikerUpdateServlet" method="post">
                <p>
                    <label>Naam:</label>
                    <input type="text" name="txtNaam" value="" required>
                </p>
                <p>
                    <label>Paswoord:</label>
                    <input type="text" name="txtPaswoord1" value=" " required>
                </p>
                <p>
                    <label>Paswoord controle:</label>
                    <input type="text" name="txtPaswoord2" value=" " required>
                </p>
                <p>
                    <label>SLA:</label>
                    <select name="selectSLA" required>
                        <% for(Sla s:SLAlist){ %>
                        <option value=""><%=s.getOmschrijving()%></option>
                        
                        <%}%>
                    </select>

                </p>
                <p>
                    <label>Rol:</label>
                    <select name="selectRol" required>
                        <% for(Role s:Rollist){ %>
                        <option value=""><%=s.getOmschrijving()%></option>
                        <%}%>
                    </select>
                </p>            
                <p>
                    <label>Bedrijf:</label>           
                    <select name="selectBedrijf" required>
                        <% for(Bedrijf s:Bedrijflist){ %>
                        <option value=""><%=s.getNaam()%></option>
                        <%}%>
                    </select>
                </p>

                <input type="submit" name="btnAanmaken" value="Aanmaken">
                <input type="submit" name="btnAnnuleren" value="Annuleren">
            </form>
        </div>
    </body>
</html>
