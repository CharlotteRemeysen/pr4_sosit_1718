/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www;

import hbo5.it.www.beans.Ticket;
import hbo5.it.www.dataaccess.DABericht;
import hbo5.it.www.dataaccess.DASubTicketMap;
import hbo5.it.www.dataaccess.DATicket;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;


/**
 *
 * @author davy
 */
@WebServlet(name = "AanmaakSubticketServlet", urlPatterns = {"/AanmaakSubticketServlet"})
public class AanmaakSubticketServlet extends HttpServlet {
    
    DASubTicketMap daSubTicketMap = null;
    DATicket daTicket = null;
    @Resource(name="jdbc/projectTIM")
    private DataSource dataSource;
    
    
    @Override
    public void init() throws ServletException {
       
        
        //create db util and pass in the conn/ pool datasource
        
        try{
            if(daSubTicketMap == null){
                daSubTicketMap = new DASubTicketMap();
            }
            if (daTicket == null){
                daTicket = new DATicket();
            }
           
        }
        catch (Exception ex){
            throw new ServletException(ex);
        }
    
    }
   

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String theCommand = request.getParameter("command");
        try {
            if (theCommand == null){
                theCommand = "HUIDIG_TICKET";
            }
            
            //route to the appropriate method
            switch (theCommand){
                case"ADD":                    
                    addSubticket(request,response);
                    break;
                case"HUIDIG_TICKET":
                    subticketdetail(request,response);
                    break;
                case"LIST_SUBTICKET":
                    listsubticket(request,response);
                    break;                
                
                default:
                    subticketdetail(request,response);
            }
            subticketdetail(request,response);
            
            
            
        }
        catch (Exception ex) {
            throw new ServletException(ex);
        }
    
   
    }            


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void addSubticket(HttpServletRequest request, HttpServletResponse response) throws Exception {       
         
        HttpSession session = request.getSession();
        try {
        
        //read ticket info from form data
        
        int masterticketid = Integer.parseInt(request.getParameter("masterticketid"));
        int subticketid = Integer.parseInt(request.getParameter("subticketid"));
                
        Ticket masterticket =(Ticket)session.getAttribute("ticket");
        Ticket subticket = (Ticket)session.getAttribute("subticket");       
        
        //create new Subticket object        
               
        DASubTicketMap subticketmap = new DASubTicketMap();
        subticketmap.addSubticket(masterticket.getTicketid(), subticket.getTicketid());
        //add subticket to the database
        
       daSubTicketMap.addSubticket(masterticketid, subticketid);
       session.setAttribute("ticket", subticket);
        }
        catch (Exception ex){
             throw new ServletException(ex);
        }
       
       //send back to ticketdetail;
       
       response.sendRedirect("TicketDetailServlet");
              
              
    }

    private void subticketdetail (HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        //get ticket form db        
        
        int ticketId = Integer.parseInt(request.getParameter("ticketid"));
        Ticket ticket = daTicket.getTicketbyID(ticketId);
        
        // add ticket to the request
        
        request.setAttribute("HUIDIG_TICKET", ticket);
        
        //send to jsp page       
        
        response.sendRedirect("TicketDetailServlet");
        
        
    }  
    
    private void listsubticket (HttpServletRequest request, HttpServletResponse response)
		throws Exception
    {
        //get ticket from db
        
        int ticketId = Integer.parseInt(request.getParameter("ticketid"));
        
        
        // get subtickets from db util
        List<Ticket> subtickets = daSubTicketMap.getListSubTicketsbyTicketID(ticketId);
        
        // add subtickets to the request
        request.setAttribute("SUB_TICKET", subtickets);
        
        //send to jsp page
        RequestDispatcher dispatcher = request.getRequestDispatcher("/subticketlijst.jsp");
        dispatcher.forward(request, response);
    }
    
   
     

}
