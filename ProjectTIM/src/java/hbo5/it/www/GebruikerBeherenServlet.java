/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www;

import hbo5.it.www.beans.Bedrijf;
import hbo5.it.www.beans.Bericht;
import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Role;
import hbo5.it.www.beans.Sla;
import hbo5.it.www.beans.Ticket;
import hbo5.it.www.dataaccess.DABedrijf;
import hbo5.it.www.dataaccess.DABedrijf;
import hbo5.it.www.dataaccess.DABericht;
import hbo5.it.www.dataaccess.DAGebruiker;
import hbo5.it.www.dataaccess.DAGebruiker;
import hbo5.it.www.dataaccess.DARole;
import hbo5.it.www.dataaccess.DARole;
import hbo5.it.www.dataaccess.DASla;
import hbo5.it.www.dataaccess.DASla;
import hbo5.it.www.dataaccess.DATicket;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author josvj
 */
@WebServlet(name = "GebruikerBeherenServlet", urlPatterns = {"/GebruikerBeherenServlet"})
public class GebruikerBeherenServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
       
    DARole darole = null;
    DAGebruiker dagebruiker = null;
    DASla dasla = null;
    DABedrijf dabedrijf=null;
    @Override
    public void init() throws ServletException {
        try {

            if (darole == null) {
                darole = new DARole();
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
        
        try {

            if (dagebruiker == null) {
                dagebruiker = new DAGebruiker();
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }  
        
        try {

            if (dasla == null) {
                dasla = new DASla();
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }  
        
        try {

            if (dabedrijf == null) {
                dabedrijf = new DABedrijf();
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Gebruiker g = null;
        if (request.getParameter("btnAanpassen")!=null) 
        {
            
            if (request.getParameter("param1")!=null)
            {
                int gebruikerID = Integer.parseInt(request.getParameter("param1"));
                g = dagebruiker.getGebruikerbyID(gebruikerID);
            }

            ArrayList<Role> RolList = darole.returnRolList();
            ArrayList<Sla> SLAList=dasla.returnSlaList();
            ArrayList<Bedrijf> BedrijfList=dabedrijf.returnBedrijfList();

            RequestDispatcher rd = request.getRequestDispatcher("gebruikerAanpassen.jsp");
            request.setAttribute("Gebruiker", g);
            request.setAttribute("RolLijst", RolList);
            request.setAttribute("SLALijst", SLAList);
            request.setAttribute("BedrijfLijst", BedrijfList);
            rd.forward(request, response);
        }
        else if(request.getParameter("btnAanmaken")!=null)
        {
            ArrayList<Role> RolList = darole.returnRolList();
            ArrayList<Sla> SLAList=dasla.returnSlaList();
            ArrayList<Bedrijf> BedrijfList=dabedrijf.returnBedrijfList();

            RequestDispatcher rd = request.getRequestDispatcher("gebruikerAanmaken.jsp");
            request.setAttribute("RolLijst", RolList);
            request.setAttribute("SLALijst", SLAList);
            request.setAttribute("BedrijfLijst", BedrijfList);
            rd.forward(request, response);
        }
    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}