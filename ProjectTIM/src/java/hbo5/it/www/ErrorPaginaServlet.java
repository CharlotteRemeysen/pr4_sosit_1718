/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kevin
 */
@WebServlet(name = "ErrorPaginaServlet", urlPatterns = {"/ErrorPaginaServlet"})
public class ErrorPaginaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher rd = null;
        Integer errorcode = null;

        //van pagina met error naar errorpagina
        if (session.getAttribute("errorcode") != null && request.getParameter("button") == null) {
            String errormessage = "";
            errorcode = (Integer) session.getAttribute("errorcode");
            switch (errorcode) {
                case 10:
                    errormessage = "Er is geen gebruikerID of password ingevuld.";
                    break;
                case 11:
                    errormessage = "GebruikerID die ingevuld is is geen geldige gebruikerID.";
                    break;
                case 12:
                    errormessage = "GebruikerID die ingevuld is bestaat niet.";
                    break;
                case 13:
                    errormessage = "Het wachtwoord dat ingevuld is, is niet correct.";
                    break;
                case 14:
                    errormessage = "De wachtwoorden komen niet overeen.";
                    break;                    
                case 20:
                    errormessage = "Er is geen gebruiker ingesteld in de session. Bij het laden van berichtservlet.";
                    break;
                case 21:
                    errormessage = "Er is geen ticket ingesteld in de session. Bij het laden van berichtservlet.";
                    break;
                case 22:
                    errormessage = "Er is geen tekst ingevuld, bij het drukken op de knop 'Send'.";
                    break;
                case 30:
                    errormessage = "Er is geen gebruiker ingesteld in de session. Bij het laden van ticketdetailservlet.";
                    break;
                case 31:
                    errormessage = "Er is geen ticket ingesteld in de session. Bij het laden van ticketdetailservlet.";
                    break;
                case 40:
                    errormessage = "Er is geen ticket ingesteld in de session. Bij het laden van updateticketservlet.";
                    break;
                case 41:
                    errormessage = "Er is geen gebruiker ingesteld in de session. Bij het laden van updateticketservlet.";
                    break;
                case 42:
                    errormessage = "Er is geen status doorgeven in parameter vanuit ticketdetail.jsp.";
                    break;
                case 43:
                    errormessage = "Er is geen behandelaar doorgeven in parameter vanuit ticketdetail.jsp.";
                    break;

                default:
                    errormessage = "Er is iets mis gegaan, geen gekende errorcode.";

            }
            session.setAttribute("errormessage", errormessage);
            rd = request.getRequestDispatcher("errorpagina.jsp");
            rd.forward(request, response);
        }

        //van errorpagina terug gaan
        if (request.getParameter("button") != null && session.getAttribute("errorcode") != null) {
            errorcode = (Integer) session.getAttribute("errorcode");
            switch (errorcode) {
                case 10:
                case 11:
                case 12:
                case 13:
                    rd = request.getRequestDispatcher("login.jsp");
                    rd.forward(request, response);
                    break;
                case 14:
                    rd = request.getRequestDispatcher("gebruikerAanmaken.jsp");
                    rd.forward(request, response);
                    break;
                case 20:
                case 21:
                case 22:
                case 30:
                case 31:
                case 40:
                case 41:
                case 42:
                case 43:
                    response.sendRedirect("TicketDetailServlet");
                    break;
            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
