/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.beans;

/**
 *
 * @author Freedom
 */
public class Bedrijf {
    private int bedrijfid;
    private String naam;
    private String omschrijving;

    public int getBedrijfid() {
        return bedrijfid;
    }

    public String getNaam() {
        return naam;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setBedrijfid(int bedrijfid) {
        this.bedrijfid = bedrijfid;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }
    
    
    
}
