/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Bedrijf;
import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Groep;
import hbo5.it.www.beans.Role;
import hbo5.it.www.beans.Sla;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Freedom
 */
public class DAGroep {
    
    //Altijd connection aanmaken + import java.sql.Connection//
    
    private Connection connection = null;

    //Bovenstaande dingen voor elke klassen toevoegen (aangepast aan de klassen (naam constructor)//
    //********************************************************************************************//

    //Methode om een obj toe te voegen//
    public boolean insertGroep(int slaid, String omschrijving) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;

        try {
            //tablenaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("insert into groep values ((select max(groepid)+1 from groep),?,?)");
            statement.setInt(1, slaid);
            statement.setString(2, omschrijving);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                     
                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }

    // Methode om een obj te verwijderen op ID//
    public boolean deleteGroep(int id) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("delete from groep where groepid = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                     
                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }
    
    //Methode om obj aan te passen //
    public boolean updateGroep(int id, int slaid, String omschrijving){
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("update groep set slaid = ?, groepomschrijving = ? where groepid = ?");
            statement.setInt(1, slaid);
            statement.setString(2, omschrijving);
            statement.setInt(3, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                     
                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }
    
    //Methode voor obj te zoeken op ID//
    public Groep getGroepbyID(int id){
        connection = ConnectionManager.getConnection();
        Groep obj = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try{
            statement = connection.prepareStatement("select * from groep where groepid = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                obj = new Groep();
                obj.setGroepid(resultSet.getInt("groepid"));
                obj.setOmschrijving(resultSet.getString("groepomschrijving"));
                obj.setSlaid(resultSet.getInt("slaid"));
                
                DASla dasla = new DASla();
                Sla sla = dasla.getSlabyID(obj.getSlaid());
                obj.setSla(sla);
                                                          
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                     
                }
            } catch (SQLException e) {
            }
        }
        return obj;
    }
    
}
