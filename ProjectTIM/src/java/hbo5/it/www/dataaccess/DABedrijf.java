/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Bedrijf;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Freedom
 */
public class DABedrijf {
    //Altijd connection aanmaken + import java.sql.Connection//

    private Connection connection = null;

    //Bovenstaande dingen voor elke klassen toevoegen (aangepast aan de klassen (naam constructor)//
    //********************************************************************************************//
    //Methode om een obj toe te voegen//
    public boolean insertBedrijf(String naam, String omschrijving) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;

        try {
            //tabelnaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("insert into bedrijf values ((select max(bedrijfid)+1 from bedrijf),?,?)");
            statement.setString(1, naam);
            statement.setString(2, omschrijving);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }

    // Methode om een obj te verwijderen op ID//
    public boolean deleteBedrijf(int id) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("delete from bedrijf where bedrijfid = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }

    //Methode om obj aan te passen //
    public boolean updateBedrijf(int id, String omschrijving, String naam) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("update bedrijf set bedrijfomschrijving = ?, bedrijfsnaam = ? where bedrijfid = ?");
            statement.setString(1, omschrijving);
            statement.setString(2, naam);
            statement.setInt(3, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }

    //Methode voor obj te zoeken op ID//
    public Bedrijf getBedrijfbyID(int id) {
        connection = ConnectionManager.getConnection();
        Bedrijf obj = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.prepareStatement("select * from bedrijf where bedrijfid = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                obj = new Bedrijf();
                obj.setBedrijfid(resultSet.getInt("bedrijfid"));
                obj.setNaam(resultSet.getString("bedrijfsnaam"));
                obj.setOmschrijving(resultSet.getString("bedrijfomschrijving"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return obj;

    }
    
    public ArrayList<Bedrijf> returnBedrijfList()
    {
        connection = ConnectionManager.getConnection();

        ArrayList<Bedrijf> listBedrijven = new ArrayList<Bedrijf>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Bedrijf obj=null;

        try {
            statement = connection.prepareStatement("select * from BEDRIJF");
            resultSet = statement.executeQuery();

            while(resultSet.next()){
                obj = new Bedrijf();
                obj.setBedrijfid(resultSet.getInt("bedrijfid"));
                obj.setOmschrijving(resultSet.getString("bedrijfomschrijving"));
                obj.setNaam(resultSet.getString("bedrijfsnaam"));
                
                listBedrijven.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return listBedrijven;
    }

}
