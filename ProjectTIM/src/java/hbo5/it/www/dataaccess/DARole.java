/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Role;
import hbo5.it.www.beans.Ticket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Kevin
 */
public class DARole {

    //Altijd connection aanmaken + import java.sql.Connection//
    private Connection connection = null;


    //Bovenstaande dingen voor elke klassen toevoegen (aangepast aan de klassen (naam constructor)//
    //********************************************************************************************//

    //Methode om een Role toe te voegen//
    public boolean insertRole(String omschrijving) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;

        try {
            //tabelnaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("insert into role values ((select max(roleid)+1 from role),?)");
            statement.setString(1, omschrijving);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }

    // Methode om een Role te verwijderen op ID//
    public boolean deleteRole(int id) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("delete from role where roleid = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }
    
    //Methode om Role aan te passen //
    public boolean updateRole(int id, String omschrijving){
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("update role set roleomschrijving = ? where roleid = ?");
            statement.setString(1, omschrijving);
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                     
                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }
    
    //Methode voor role te zoeken op ID//
    public Role getRolebyID(int id){
        connection = ConnectionManager.getConnection();
        Role obj = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try{
            statement = connection.prepareStatement("select * from role where roleid = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                obj = new Role();
                obj.setRoleid(resultSet.getInt("roleid"));
                obj.setOmschrijving(resultSet.getString("roleomschrijving"));                
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return obj;
    }
    
    public ArrayList<Role> returnRolList()
    {
        connection = ConnectionManager.getConnection();

        ArrayList<Role> listRollen = new ArrayList<Role>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Role obj=null;

        try {
            statement = connection.prepareStatement("select * from role");
            resultSet = statement.executeQuery();

            while(resultSet.next()){
                obj = new Role();
                obj.setRoleid(resultSet.getInt("roleid"));
                obj.setOmschrijving(resultSet.getString("roleomschrijving"));
                
                listRollen.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
        }
        return listRollen;
    }
}
