/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Asset;
import hbo5.it.www.beans.Groep;
import hbo5.it.www.beans.Sla;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kevin
 */
public class DATicketAssetMap {
    

    //Altijd connection aanmaken + import java.sql.Connection//
    private Connection connection = null;

    //Bovenstaande dingen voor elke klassen toevoegen (aangepast aan de klassen (naam constructor)//
    //********************************************************************************************//
    
    public ArrayList<Asset> getListAssetsbyTicketID(int ticketid)
    {
        connection = ConnectionManager.getConnection();
        ArrayList<Asset> resultaat = new ArrayList<Asset>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //tablenaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("select * from ticketassetmap where ticketid = ?");
            statement.setInt(1, ticketid);
            resultSet = statement.executeQuery();
            while(resultSet.next()){
                int assetid = resultSet.getInt("assetid");
                
                DAAsset daasset = new DAAsset();
                Asset asset = daasset.getAssetbyID(assetid);
                resultaat = new ArrayList<Asset>();
                resultaat.add(asset);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();
                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }
}
