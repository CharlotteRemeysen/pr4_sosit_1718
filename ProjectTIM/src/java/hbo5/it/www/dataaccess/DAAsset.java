/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Asset;
import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Groep;
import hbo5.it.www.beans.Sla;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Freedom
 */
public class DAAsset {

    private Connection connection = null;

    //Constructor aanmaken met inloggegevens//
    public DAAsset() throws ClassNotFoundException, SQLException {

    }

    //Bovenstaande dingen voor elke klassen toevoegen (aangepast aan de klassen (naam constructor)//
    //********************************************************************************************//
    //Methode om een obj toe te voegen//
    public boolean insertAsset(int beheerderid, int groepid, String naam, String omschrijving) {
        connection = ConnectionManager.getConnection();

        boolean resultaat = true;
        PreparedStatement statement = null;

        try {
            //tablenaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("insert into asset values ((select max(assetid)+1 from asset),?,?,?,?)");
            statement.setInt(1, beheerderid);
            statement.setInt(2, groepid);
            statement.setString(3, naam);
            statement.setString(4, omschrijving);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }

    // Methode om een obj te verwijderen op ID//
    public boolean deleteAsset(int id) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("delete from asset where assetid = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }

    //Methode om obj aan te passen //
    public boolean updateAsset(int id, int beheerderid, int groepid, String naam, String omschrijving) {
        connection = ConnectionManager.getConnection();
        boolean resultaat = true;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("update asset set beheerderid = ?, groepid = ?, assetnaam=?, assetomschrijving = ? where assetid = ?");
            statement.setInt(1, beheerderid);
            statement.setInt(2, groepid);
            statement.setString(3, naam);
            statement.setString(4, omschrijving);
            statement.setInt(5, id);
            statement.executeUpdate();
        } catch (Exception e) {
            resultaat = false;
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return resultaat;
    }

    //Methode voor obj te zoeken op ID//
    public Asset getAssetbyID(int id) {
        connection = ConnectionManager.getConnection();
        Asset obj = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.prepareStatement("select * from asset where assetid = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                obj = new Asset();
                obj.setAssetid(resultSet.getInt("assetid"));
                obj.setNaam(resultSet.getString("assetnaam"));
                obj.setOmschrijving(resultSet.getString("assetomschrijving"));
                obj.setBeheerderid(resultSet.getInt("beheerderid"));
                obj.setGroepid(resultSet.getInt("groepid"));

                DAGebruiker dagebruiker = new DAGebruiker();
                Gebruiker beheerder = dagebruiker.getGebruikerbyID(obj.getBeheerderid());
                obj.setBeheerder(beheerder);

                DAGroep dagroep = new DAGroep();
                Groep groep = dagroep.getGroepbyID(obj.getGroepid());
                obj.setGroep(groep);

                DASubAssetMap dasubassets = new DASubAssetMap();
                ArrayList<Asset> listsubassets = dasubassets.getListSubAssetsbyAssetID(obj.getAssetid());
                obj.setListassets(listsubassets);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
        }
        return obj;
    }

}
