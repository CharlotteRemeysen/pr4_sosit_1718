/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www.dataaccess;

import hbo5.it.www.beans.Bericht;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Freedom
 */
public class DABerichtTicketMap {
    //Altijd connection aanmaken + import java.sql.Connection//

    private Connection connection = null;

    public ArrayList<Bericht> getBerichtenbyTicketID(int ticketid) {
        connection = ConnectionManager.getConnection();
        ArrayList<Bericht> resultaat = new ArrayList<Bericht>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //tablenaam_seg.nextval is voor het volgende getal te pakken in de database(unique id)//
            statement = connection.prepareStatement("select * from bericht where ticketid = ? order by berichtid");
            statement.setInt(1, ticketid);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int berichtid = resultSet.getInt("berichtid");
                DABericht dabericht = new DABericht();
                Bericht bericht = dabericht.getBerichtbyID(berichtid);
                resultaat.add(bericht);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                    connection.close();

                }
            } catch (SQLException e) {
            }
            return resultaat;
        }

    }

}
