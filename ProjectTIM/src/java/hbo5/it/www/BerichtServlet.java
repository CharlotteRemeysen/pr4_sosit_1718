/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www;

import hbo5.it.www.beans.Bericht;
import hbo5.it.www.beans.Gebruiker;
import hbo5.it.www.beans.Ticket;
import hbo5.it.www.dataaccess.DABericht;
import hbo5.it.www.dataaccess.DAGebruiker;
import hbo5.it.www.dataaccess.DATicket;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Freedom
 */


@WebServlet(name = "BerichtServlet",
        urlPatterns = {"/BerichtServlet"})


public class BerichtServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Ticket ticket = null;
    DATicket daticket = null;
    Bericht bericht = null;
    DABericht dabericht = null;

    @Override
    public void init() throws ServletException {
        try {

            if (daticket == null) {
                daticket = new DATicket();
            }
            if (dabericht == null) {
                dabericht = new DABericht();
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Boolean isintern;

        try {
            if (session.getAttribute("gebruiker") != null && session.getAttribute("ticket") != null && !request.getParameter("berichttekst").isEmpty()) {
                //ticket van session local zettten
                ticket = (Ticket) session.getAttribute("ticket");
                //waarde van tekst bericht in var steken
                String berichttekst = (String) request.getParameter("berichttekst");
                //gebruiker van session local zetten
                Gebruiker gebruiker = (Gebruiker) session.getAttribute("gebruiker");
                //kijken of het een intern of extern bericht is
                isintern = request.getParameter("isintern") != null;

                //kijken wat de role is van gebruiker
                // als gebruiker klant is ALTIJD extern bericht, klant kan geen intern bericht sturen 
                if (gebruiker.getRoleid() > 1) {
                    //role = behandelaar of beheerder
                    dabericht.insertBericht(gebruiker.getLoginid(), ticket.getTicketid(), berichttekst, isintern);
                } else {
                    // role = klant
                    dabericht.insertBericht(gebruiker.getLoginid(), ticket.getTicketid(), berichttekst, false);
                }
                // ticket opnieuw laden uit database en terug meegeven in session
                // anders is bericht niet toegevoegt in de list van berichten (ouwe data)
                DATicket daticket = new DATicket();
                Ticket newticket = daticket.getTicketbyID(ticket.getTicketid());
                session.setAttribute("ticket", newticket);
                // doorsturen naar servlet om ticketdetailpagina opnieuw te laden
                response.sendRedirect("TicketDetailServlet");

            } else {
                //kijken of gebruiker is ingesteld
                if (session.getAttribute("gebruiker") == null) {
                    session.setAttribute("errorcode", 20);
                    throw new Exception();
                } 
                //kijken of ticket is ingesteld
                if (session.getAttribute("ticket") == null) {
                    session.setAttribute("errorcode", 21);
                    throw new Exception();
                }
                //kijken of berichttekst is ingevuld
                if (request.getParameter("berichttekst").isEmpty()) {
                    session.setAttribute("errorcode", 22);
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            response.sendRedirect("ErrorPaginaServlet");
        }    
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
