/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hbo5.it.www;

import hbo5.it.www.beans.Ticket;
import hbo5.it.www.dataaccess.DASubTicketMap;
import hbo5.it.www.dataaccess.DATicket;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author bergh
 */
@WebServlet(name = "SubticketaddServlet", urlPatterns = {"/SubticketaddServlet"})
public class SubticketaddServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        RequestDispatcher rd = null;  
            if (session.getAttribute("ticket") != null && request.getParameter("SubTicketID") != null) {
                String tussenuitkomst = request.getParameter("SubTicketID");
                int subticketid = Integer.parseInt(tussenuitkomst);
                Ticket t = (Ticket) session.getAttribute("ticket");

                DASubTicketMap dasubticketmap = new DASubTicketMap();
                DATicket daticket = new DATicket();
                ArrayList<Integer> listids = daticket.getListTicketIDs();

                for (int i : listids) {
                    // check if opend ticket is not the ticketid you want to use and that subticketid is pickt from database as entry
                    if (i == subticketid || subticketid != t.getTicketid()) {

                        dasubticketmap.insertSubticket(t.getTicketid(), subticketid);

                    } else {
                        String error = "fout";
                    }
                }

                Ticket newticket = daticket.getTicketbyID(t.getTicketid());
                session.setAttribute("ticket", newticket);

                response.sendRedirect("TicketDetailServlet");
            } else {
                rd = request.getRequestDispatcher("/dashboard.jsp");
                rd.forward(request, response);
            }
    }

    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
